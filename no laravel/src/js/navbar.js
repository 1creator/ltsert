/*
* Sticky navbar
* */

const navbar = document.querySelector('nav.navbar');
let lastPosY = 0;

window.addEventListener('scroll', function (e) {
    if (this.scrollY > 77) {
        navbar.classList.toggle('minimized', this.scrollY > lastPosY);
        lastPosY = this.scrollY;
        navbar.style.zIndex  = 15;
    }
    if (this.scrollY === 0) {
        navbar.style.zIndex  = 10;
    }
});

/*
* Search in navbar
* */

let navbarTopContent = document.getElementsByClassName('navbar__top-content')[0];
let navbarSearchField = document.getElementsByClassName('navbar__search-field')[0];

function escKeyListener(event) {
    event = event || window.event;
    if (event.keyCode === 27) {
        navbarSearchField.value = '';
        navbarSearchField.blur();
    }
}

navbarSearchField.addEventListener('focus', function () {
    navbarTopContent.classList.toggle('navbar__top-content_active', true);
    document.addEventListener('keydown', escKeyListener);
});

navbarSearchField.addEventListener('blur', function () {
    navbarTopContent.classList.toggle('navbar__top-content_active', this.value);
    document.removeEventListener('keydown', escKeyListener);
});