(function (modules) {
    var installedModules = {};

    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) {
            return installedModules[moduleId].exports
        }
        var module = installedModules[moduleId] = {i: moduleId, l: false, exports: {}};
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
        module.l = true;
        return module.exports
    }

    __webpack_require__.m = modules;
    __webpack_require__.c = installedModules;
    __webpack_require__.d = function (exports, name, getter) {
        if (!__webpack_require__.o(exports, name)) {
            Object.defineProperty(exports, name, {enumerable: true, get: getter})
        }
    };
    __webpack_require__.r = function (exports) {
        if (typeof Symbol !== "undefined" && Symbol.toStringTag) {
            Object.defineProperty(exports, Symbol.toStringTag, {value: "Module"})
        }
        Object.defineProperty(exports, "__esModule", {value: true})
    };
    __webpack_require__.t = function (value, mode) {
        if (mode & 1) value = __webpack_require__(value);
        if (mode & 8) return value;
        if (mode & 4 && typeof value === "object" && value && value.__esModule) return value;
        var ns = Object.create(null);
        __webpack_require__.r(ns);
        Object.defineProperty(ns, "default", {enumerable: true, value: value});
        if (mode & 2 && typeof value != "string") for (var key in value) __webpack_require__.d(ns, key, function (key) {
            return value[key]
        }.bind(null, key));
        return ns
    };
    __webpack_require__.n = function (module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module["default"]
        } : function getModuleExports() {
            return module
        };
        __webpack_require__.d(getter, "a", getter);
        return getter
    };
    __webpack_require__.o = function (object, property) {
        return Object.prototype.hasOwnProperty.call(object, property)
    };
    __webpack_require__.p = "/";
    return __webpack_require__(__webpack_require__.s = 0)
})([function (module, exports, __webpack_require__) {
    __webpack_require__(1);
    __webpack_require__(4);
    module.exports = __webpack_require__(11)
}, function (module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    var _initSwipers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
    var _initSwipers__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(_initSwipers__WEBPACK_IMPORTED_MODULE_0__);
    var _navbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
    var _navbar__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(_navbar__WEBPACK_IMPORTED_MODULE_1__)
}, function (module, exports) {
    Swiper.instances = {};
    var swipers = document.getElementsByClassName("swiper-container");
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;
    try {
        for (var _iterator = swipers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var swiper = _step.value;
            var slidesPerView = parseInt(swiper.dataset.slidesperview || 1);
            var spaceBetween = parseInt(swiper.dataset.spacebetween || 0);
            var nextButton = swiper.dataset.next;
            var prevButton = swiper.dataset.prev;
            var paginationEl = swiper.dataset.paginationel;
            var paginationType = swiper.dataset.paginationtype;
            var direction = swiper.dataset.direction || "horizontal";
            var name = swiper.dataset.name;
            var allowTouchMove = swiper.dataset.allowtouchmove !== "false";
            var mousewheel = swiper.dataset.mousewheel ? {invert: false, releaseOnEdges: false} : false;
            var options = {
                slidesPerView: slidesPerView,
                spaceBetween: spaceBetween,
                allowTouchMove: allowTouchMove,
                direction: direction,
                mousewheel: mousewheel,
                navigation: {nextEl: nextButton, prevEl: prevButton},
                pagination: {el: paginationEl, type: paginationType, clickable: true}
            };
            var sw = new Swiper(swiper, options);
            if (name) {
                Swiper.instances[name] = sw
            }
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
                _iterator["return"]()
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError
            }
        }
    }
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;
    try {
        for (var _iterator2 = swipers[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var _swiper = _step2.value;
            var _name = _swiper.dataset.name;
            var controlSwiper = _swiper.dataset.controlswiper;
            if (controlSwiper) {
                Swiper.instances[_name].controller.control = Swiper.instances[controlSwiper]
            }
        }
    } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err
    } finally {
        try {
            if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
                _iterator2["return"]()
            }
        } finally {
            if (_didIteratorError2) {
                throw _iteratorError2
            }
        }
    }
}, function (module, exports) {
    var navbar = document.querySelector("nav.navbar");
    var lastPosY = 0;
    window.addEventListener("scroll", (function (e) {
        if (this.scrollY > 77) {
            navbar.classList.toggle("minimized", this.scrollY > lastPosY);
            lastPosY = this.scrollY;
            navbar.style.zIndex = 15
        }
        if (this.scrollY === 0) {
            navbar.style.zIndex = 10
        }
    }));
    var navbarTopContent = document.getElementsByClassName("navbar__top-content")[0];
    var navbarSearchField = document.getElementsByClassName("navbar__search-field")[0];

    function escKeyListener(event) {
        event = event || window.event;
        if (event.keyCode === 27) {
            navbarSearchField.value = "";
            navbarSearchField.blur()
        }
    }

    navbarSearchField.addEventListener("focus", (function () {
        navbarTopContent.classList.toggle("navbar__top-content_active", true);
        document.addEventListener("keydown", escKeyListener)
    }));
    navbarSearchField.addEventListener("blur", (function () {
        navbarTopContent.classList.toggle("navbar__top-content_active", this.value);
        document.removeEventListener("keydown", escKeyListener)
    }))
}, function (module, exports) {
}, , , , , , , function (module, exports) {
}]);