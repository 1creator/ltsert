<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/uikit', function () {
    return view('uikit');
});

Route::get('/', function () {
    return view('index');
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/service', function () {
    return view('service');
});

Route::get('/projects', function () {
    return view('projects');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/404', function () {
    return view('404');
});

Route::get('/project', function () {
    return view('project');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/partners', function () {
    return view('partners');
});

Route::get('/reviews', function () {
    return view('reviews');
});

Route::get('/article', function () {
    return view('article');
});

Route::get('/welders', function () {
    return view('welders');
});

Route::get('/contacts', function () {
    return view('contacts');
});

Route::get('/blog', function () {
    return view('blog');
});