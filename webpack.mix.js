const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.config.fileLoaderDirs.fonts = 'fonts';
mix.options({
    terser: {
        terserOptions: {
            compress: false,
            mangle: false,
            sourceMap: false,
            keep_fnames: true,
            keep_classnames: true,
        },
    }
});
mix
    .webpackConfig({
        resolve: {
            modules: [
                path.resolve(__dirname, 'node_modules'),
                path.resolve(__dirname, './resources'),
            ]
        }
    })
    .options({processCssUrls: false})
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/vendor.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css');

mix.disableSuccessNotifications();