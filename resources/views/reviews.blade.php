@extends('layout')
@push('head')
    <title>Отзывы</title>
@endpush
@section('content')
    <section class="entry-block entry-block_sm reviews__entry-block overflow-hidden">
        <div class="container h-100">
            <div class="row h-100 position-relative">
                <div class="col-12 col-md-6 align-self-center">
                    <h1 class="h2 mb-3 text-uppercase">
                        Отзывы о нас.
                    </h1>
                    <p class="h4 text-uppercase font-weight-normal">
                        Наши горячо любимые партнеры
                        и&nbsp;клиенты не смогли не поделиться своими впечатлениями от сотрудничества
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <img class="reviews__entry-block-img" src="/images/reviews/entry-bg.png">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <img class="reviews__entry-block-img" src="/images/reviews/entry-bg.png">
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
    'Главная'=>'/',
    'Отзывы'=>'/reviews',
    ]])
    <section class="page-block">
        <div class="reviews-cards container">
            <div class="row">
                <div class="col-12 mb-6">
                    <blockquote class="review-card review-card_combined">
                        <a class="review-card__left" data-fancybox href="/images/project/review.jpg">
                            <div class="review-card__image"
                                 style="background-image: url('/images/project/review.jpg');">
                            </div>
                            <div class="review-card__gradient"></div>
                            <div class="review-card__date">08.11.2019</div>
                        </a>
                        <div class="review-card__right">
                            <p class="review-card__right-text">За время работы «ЛенТехСертификация» зарекомендовала
                                себя
                                как компетентная, ответственная и исполнительная компания. Работы по сертификации
                                выполнялись качественно и в согласованные сроки.
                            </p>
                            <footer class="review-card__right-footer">
                                <div class="subtitle-sm mb-4">
                                    <span>Благодарственное письмо</span> <cite class="font-normal">АО «Валмет
                                        Автоматизация»</cite>
                                </div>
                                <a href="#" class="d-block text-md-right">Смотреть реализованный проект этого
                                    клиента <i class="fas fa-chevron-right"></i></a>
                            </footer>
                        </div>
                    </blockquote>
                </div>
                <div class="col-12 mb-6">
                    <blockquote class="review-card review-card_combined">
                        <a class="review-card__left" data-fancybox href="/images/project/review.jpg">
                            <div class="review-card__image"
                                 style="background-image: url('/images/project/review.jpg');">
                            </div>
                            <div class="review-card__gradient"></div>
                            <div class="review-card__date">08.11.2019</div>
                        </a>
                        <div class="review-card__right">
                            <p class="review-card__right-text">За время работы «ЛенТехСертификация» зарекомендовала
                                себя
                                как компетентная, ответственная и исполнительная компания. Работы по сертификации
                                выполнялись качественно и в согласованные сроки.
                            </p>
                            <footer class="review-card__right-footer">
                                <div class="subtitle-sm mb-4">
                                    <span>Благодарственное письмо</span> <cite class="font-normal">АО «Валмет
                                        Автоматизация»</cite>
                                </div>
                                <a href="#" class="d-block text-md-right">Смотреть реализованный проект этого
                                    клиента <i class="fas fa-chevron-right"></i></a>
                            </footer>
                        </div>
                    </blockquote>
                </div>
                <div class="col-12 col-lg-4 mb-6 mb-md-0">
                    <a class="review-card review-card_image"
                       data-fancybox href="/images/service/review-image.png">
                        <div class="review-card__image"
                             style="background-image: url('/images/service/review-image.png')">
                        </div>
                        <div class="review-card__gradient"></div>
                        <div class="review-card__date">08.11.2019</div>
                    </a>
                </div>
                <div class="col-12 col-lg-4 mb-6 mb-md-0">
                    <a class="review-card review-card_image" data-fancybox href="/images/service/review-image.png">
                        <div class="review-card__image"
                             style="background-image: url('/images/service/review-image.png')">
                        </div>
                        <div class="review-card__gradient"></div>
                        <div class="review-card__date">08.11.2019</div>
                    </a>
                </div>
                <div class="col-12 col-lg-4 mb-6 mb-md-0">
                    <div class="review-card review-card_video"
                         style="background-image: url('/images/service/review-video-preview.jpg')">
                        <div class="review-card__date">08.11.2019</div>
                        <a data-fancybox href="https://www.youtube.com/watch?v=uJo6GnDYi7Y">
                            <button class="fas fa-play-circle review-card__play-btn"></button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('components.callback-primary')
    @include('components.director')
    <div class="mb-7"></div>
    @include('components.promo-cert')
    @include('components.feedback')
@endsection
