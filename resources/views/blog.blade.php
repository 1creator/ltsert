@extends('layout')
@push('head')
    <title>Блог</title>
@endpush
@section('content')
    <section class="entry-block entry-block_sm blog__entry-block overflow-hidden">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-12 col-md-5 text-uppercase">
                    <h1 class="h2 mb-3 mr-md-n5">
                        БЛОГ.
                    </h1>
                    <p class="h4 font-weight-normal text-uppercase">Авторы материалов —
                        исключительно наши штатные копирайтеры
                    </p>
                </div>
                <div class="col-12 col-md-7 text-center d-none d-md-flex position-static">
                    <img class="" src="/images/faq/entry-girl.png">
                </div>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Блог'=>'/blog',
    ]])
    <section class="page-block container">
        <div class="input input_icon input_sm input_bg-gradient">
            <i class="fas fa-search text-primary"></i>
            <input type="text" placeholder="Поиск">
        </div>
        <div class="row mb-7">
            <div class="col-12 col-md-8 mb-5 mb-md-0">
                <div class="row">
                    <div class="col-12 col-md-6 mb-5">
                        <article class="article-card article-card_gradient">
                            <div class="article-card__img"
                                 style="background-image: url('/images/index/article-2.jpg')"
                            >
                            </div>
                            <div class="article-card__top-wrap">
                                <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                                <div class="article-card__company">
                                    <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                                </div>
                                <div class="article-card__read-time">
                                    <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                                </div>
                            </div>
                            <h3 class="article-card__title article-card__title_category">
                                <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                                <span class="article-card__category">
                                    <i class="fas fa-folder-open mr-2"></i> Подтверждение соответствия
                                </span>
                            </h3>
                            <p class="article-card__text">
                                Какие нормативные акты действуют в отношении различных видов трансформаторов? Как
                                получить
                                сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты
                                компании...
                            </p>
                        </article>
                    </div>
                    <div class="col-12 col-md-6 mb-5">
                        <article class="article-card article-card_gradient">
                            <div class="article-card__img"
                                 style="background-image: url('/images/index/article-2.jpg')"
                            >
                            </div>
                            <div class="article-card__top-wrap">
                                <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                                <div class="article-card__company">
                                    <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                                </div>
                                <div class="article-card__read-time">
                                    <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                                </div>
                            </div>
                            <h3 class="article-card__title article-card__title_category">
                                <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                                <span class="article-card__category">
                                    <i class="fas fa-folder-open mr-2"></i> Подтверждение соответствия
                                </span>
                            </h3>
                            <p class="article-card__text">
                                Какие нормативные акты действуют в отношении различных видов трансформаторов? Как
                                получить
                                сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты
                                компании...
                            </p>
                        </article>
                    </div>
                    <div class="col-12 col-md-6 mb-5">
                        <article class="article-card article-card_gradient">
                            <div class="article-card__img"
                                 style="background-image: url('/images/index/article-2.jpg')"
                            >
                            </div>
                            <div class="article-card__top-wrap">
                                <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                                <div class="article-card__company">
                                    <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                                </div>
                                <div class="article-card__read-time">
                                    <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                                </div>
                            </div>
                            <h3 class="article-card__title article-card__title_category">
                                <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                                <span class="article-card__category">
                                    <i class="fas fa-folder-open mr-2"></i> Подтверждение соответствия
                                </span>
                            </h3>
                            <p class="article-card__text">
                                Какие нормативные акты действуют в отношении различных видов трансформаторов? Как
                                получить
                                сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты
                                компании...
                            </p>
                        </article>
                    </div>
                    <div class="col-12 col-md-6 mb-5">
                        <article class="article-card article-card_gradient">
                            <div class="article-card__img"
                                 style="background-image: url('/images/index/article-2.jpg')"
                            >
                            </div>
                            <div class="article-card__top-wrap">
                                <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                                <div class="article-card__company">
                                    <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                                </div>
                                <div class="article-card__read-time">
                                    <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                                </div>
                            </div>
                            <h3 class="article-card__title article-card__title_category">
                                <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                                <span class="article-card__category">
                                    <i class="fas fa-folder-open mr-2"></i> Подтверждение соответствия
                                </span>
                            </h3>
                            <p class="article-card__text">
                                Какие нормативные акты действуют в отношении различных видов трансформаторов? Как
                                получить
                                сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты
                                компании...
                            </p>
                        </article>
                    </div>
                    <div class="col-12 col-md-6 mb-5 mb-md-0">
                        <article class="article-card article-card_gradient">
                            <div class="article-card__img"
                                 style="background-image: url('/images/index/article-2.jpg')"
                            >
                            </div>
                            <div class="article-card__top-wrap">
                                <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                                <div class="article-card__company">
                                    <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                                </div>
                                <div class="article-card__read-time">
                                    <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                                </div>
                            </div>
                            <h3 class="article-card__title article-card__title_category">
                                <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                                <span class="article-card__category">
                                    <i class="fas fa-folder-open mr-2"></i> Подтверждение соответствия
                                </span>
                            </h3>
                            <p class="article-card__text">
                                Какие нормативные акты действуют в отношении различных видов трансформаторов? Как
                                получить
                                сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты
                                компании...
                            </p>
                        </article>
                    </div>
                    <div class="col-12 col-md-6">
                        <article class="article-card article-card_gradient">
                            <div class="article-card__img"
                                 style="background-image: url('/images/index/article-2.jpg')"
                            >
                            </div>
                            <div class="article-card__top-wrap">
                                <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                                <div class="article-card__company">
                                    <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                                </div>
                                <div class="article-card__read-time">
                                    <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                                </div>
                            </div>
                            <h3 class="article-card__title article-card__title_category">
                                <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                                <span class="article-card__category">
                                    <i class="fas fa-folder-open mr-2"></i> Подтверждение соответствия
                                </span>
                            </h3>
                            <p class="article-card__text">
                                Какие нормативные акты действуют в отношении различных видов трансформаторов? Как
                                получить
                                сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты
                                компании...
                            </p>
                        </article>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <aside class="sidebar">
                    <div class="row mb-4">
                        <div class="col-12">
                            <ul class="nav tabs tabs_outline-blue w-100 flex-column flex-md-row">
                                <li>
                                    <a class="tabs__item nav-link active" data-toggle="tab"
                                       href="#packageTab1">Популярные</a>
                                </li>
                                <li>
                                    <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab2">Рубрики</a>
                                </li>
                                <li>
                                    <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab3">Архивы</a>
                                </li>
                                <li>
                                    <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab4">Теги</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="packageTab1">
                            <div>
                                <h4 class="mb-3">Популярные записи</h4>
                                <div class="sidebar__anonce p-3 mb-5">
                                    <div class="article-row mb-3">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="article-row">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="packageTab2">
                            <div>
                                <h4 class="mb-3">Рубрики</h4>
                                <div class="sidebar__anonce p-3 mb-5">
                                    <div class="article-row mb-3">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="article-row">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="packageTab3">
                            <div>
                                <h4 class="mb-3">Архивы</h4>
                                <div class="sidebar__anonce p-3 mb-5">
                                    <div class="article-row mb-3">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="article-row">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="packageTab4">
                            <div>
                                <h4 class="mb-3">Теги</h4>
                                <div class="sidebar__anonce p-3 mb-5">
                                    <div class="article-row mb-3">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="article-row">
                                        <img class="article-row__image" src="/images/article/popular.jpg">
                                        <div class="article-row__text">
                                            <div class="text-primary">
                                                <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                            </div>
                                            <h4 class="sidebar__anonce-link mt-2">
                                                <a href="#" class="text-dark">Сертификация трансформаторов:
                                                    нормативные
                                                    акты,
                                                    необходимые документы...
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-6">
                        <h4 class="mb-3">Новые записи</h4>
                        <div class="article-row mb-3">
                            <img class="article-row__image" src="/images/article/popular.jpg">
                            <div class="article-row__text">
                                <div class="text-gray-light">
                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                </div>
                                <h4 class="sidebar__anonce-link mt-2">
                                    <a href="#" class="text-dark">Сертификация трансформаторов: нормативные акты,
                                        необходимые документы...
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="article-row mb-3">
                            <img class="article-row__image" src="/images/article/popular.jpg">
                            <div class="article-row__text">
                                <div class="text-gray-light">
                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                </div>
                                <h4 class="sidebar__anonce-link mt-2">
                                    <a href="#" class="text-dark">Сертификация трансформаторов: нормативные акты,
                                        необходимые документы...
                                    </a>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="mb-5">
                        <h4 class="mb-3">Свежие комментарии</h4>
                        <div class="sidebar__comments mb-4 font-weight-medium">
                            <div class="mb-3">
                                <span class="text-gray">Человек Паук</span> прокоментировал запись <br>
                                <a class="text-primary-light"
                                   href="#">«Российский морской регистр судоходства»</a>
                            </div>
                            <div class="mb-3">
                                <span class="text-primary">Дед Пул</span> прокоментировал запись <br> <a
                                        class="text-primary-light"
                                        href="#">«Российский морской регистр судоходства»</a>
                            </div>
                            <div class="mb-3">
                                <span class="text-gray">Человек Паук</span> прокоментировал запись <br>
                                <a class="text-primary-light"
                                   href="#">«Российский морской регистр судоходства»</a>
                            </div>
                            <div>
                                <span class="text-primary">Дед Пул</span> прокоментировал запись <br> <a
                                        class="text-primary-light"
                                        href="#">«Российский морской регистр судоходства»</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h4 class="mb-3">Рубрики</h4>
                        <div class="tab-content__rubrics mb-4">
                            <h4 class="mb-0">
                                <a href="#" class="btn-rubrics mb-4">Добровольная сертификация</a>
                            </h4>
                            <h4 class="mb-0">
                                <a href="#" class="btn-rubrics mb-4">Негосударственная экспертиза</a>
                            </h4>
                            <h4 class="mb-0">
                                <a href="#" class="btn-rubrics mb-4">Подтверждение соответствия</a>
                            </h4>
                            <h4 class="mb-0">
                                <a href="#" class="btn-rubrics mb-4">Сертификация экспорта</a>
                            </h4>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
        <ul class="pagination">
            <li>
                <a class="pagination__item pagination__item_prev" href="#"></a>
            </li>
            <li>
                <a class="pagination__item" href="#">1</a>
            </li>
            <li>
                <a class="pagination__item" href="#">2</a>
            </li>
            <li>
                <a class="pagination__item pagination__item_current">3</a>
            </li>
            <li>
                <a class="pagination__item" href="#">4</a>
            </li>
            <li>
                <a class="pagination__item pagination__item_next" href="#"></a>
            </li>
        </ul>
    </section>
    @include('components.callback-primary')
    @include('components.director')
    <div class="mb-7"></div>
    @include('components.promo-cert')
    @include('components.feedback')
@endsection