@extends('layout')
@push('head')
    <title>Статья</title>
@endpush
@section('content')
    <section style="background-image:url('/images/article/Background.jpg')"
             class="entry-block entry-block_sm">
        <div class="container h-100 pt-7">
            <div class="row mb-4">
                <div class="col-md-2 text-white">08.11.2019</div>
                <div class="col-md-2 subtitle-sm text-secondary"><i class="fas fa-users mr-2"></i> ЛенТехСертификация
                </div>
            </div>
            <div class="row align-items-center mb-md-5">
                <div class="col-12 col-md-9 text-uppercase">
                    <h1 class="h2 mb-3 mr-md-n5 text-white" style="line-height: 150%;">
                        Обучение по охране труда:
                        от&nbsp;безопасности сотрудников к&nbsp;соблюдению закона
                    </h1>

                </div>
            </div>
            <div class="row">
                <div class="col-md-3 subtitle-sm text-secondary"><i class="fas fa-folder-open"></i> Подтверждение
                    соответствия
                </div>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
    'Главная'=>'/',
    'Блог'=>'/blog',
    ' Обучение по охране труда: от безопасности сотрудников к соблюдению закона'=>'/',
    ]])
    <div class="page-block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 pt-md-6 mb-5 mb-md-0">
                    <div class="article__pdf-wrapper mb-5">
                        <div class="row">
                            <div class="col-md-6 mb-3 mb-md-0">
                                <a class="d-flex align-items-center" href="#">
                                    <i class="far fa-file-pdf article__pdf-icon"></i>
                                    <div>Мелочь, а приятно: младая поросль матереет
                                        <i class="fas fa-download"></i></div>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a class="d-flex align-items-center" href="#">
                                    <i class="far fa-file-pdf article__pdf-icon"></i>
                                    <div>Мелочь, а приятно: младая поросль матереет
                                        <i class="fas fa-chevron-right"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <article class="mb-7 article">
                        <h1>H1 SuperHead 72</h1>
                        <h2>H2 Head 48 Bold</h2>
                        <h3>H3 Headline 36 Bold</h3>
                        <h4>
                            H4 Headline 24 Bold
                        </h4>
                        <h5>
                            H5 Headline 20 Regular
                        </h5>
                        <h6>
                            H6 HEADLINE 18 MEDIUM
                        </h6>
                        <p>Значимость этих проблем настолько очевидна, что высококачественный прототип будущего
                            проекта
                            предопределяет высокую востребованность своевременного выполнения сверхзадачи. Лишь
                            активно
                            развивающиеся страны третьего мира и по сей день остаются уделом либералов, которые
                            жаждут
                            быть
                            призваны к ответу. Не следует, однако, забывать, что убеждённость некоторых оппонентов
                            позволяет
                            выполнить важные задания по разработке форм воздействия. Противоположная точка зрения
                            подразумевает, что независимые государства обнародованы. Предварительные выводы
                            неутешительны:
                            синтетическое тестирование в значительной степени обусловливает важность
                            глубокомысленных
                            рассуждений.</p>

                        <p>Сторонники тоталитаризма в науке, инициированные исключительно синтетически, своевременно
                            верифицированы. Внезапно, стремящиеся вытеснить традиционное производство,
                            нанотехнологии,
                            которые представляют собой яркий пример континентально-европейского типа политической
                            культуры,
                            будут подвергнуты целой серии независимых исследований. В своём стремлении повысить
                            качество
                            жизни, они забывают, что повышение уровня гражданского сознания напрямую зависит от
                            стандартных
                            подходов. Господа, современная методология разработки не оставляет шанса для
                            инновационных
                            методов управления процессами.</p>

                        <p>Базовые сценарии поведения пользователей могут быть подвергнуты целой серии независимых
                            исследований. Приятно, граждане, наблюдать, как некоторые особенности внутренней
                            политики,
                            превозмогая сложившуюся непростую экономическую ситуацию, объявлены нарушающими
                            общечеловеческие
                            нормы этики и морали. Для современного мира реализация намеченных плановых заданий
                            способствует
                            подготовке и реализации глубокомысленных рассуждений.</p>

                        <p>Идейные соображения высшего порядка, а также консультация с широким активом обеспечивает
                            актуальность системы массового участия. Прежде всего, высокое качество позиционных
                            исследований
                            не оставляет шанса для модели развития.</p>
                        <blockquote>
                            Идейные соображения высшего порядка, а
                            также консультация с широким активомобеспечивает актуальность системы массового участия.
                            Прежде всего, высокое качество позиционных исследований не оставляет шанса для модели
                            развития.
                        </blockquote>
                        <img src="/images/article/transformator.jpg" alt="transformator">
                        <ul>
                            <li>Сертификация ТР ТС
                                <ul>
                                    <li>Сертификация ТР ТС</li>
                                    <li>Сертификация продукции для европейского рынка (СЕ)</li>
                                    <li>Получение документов в Морском регистре
                                        <ul>
                                            <li>Сертификация ТР ТС</li>
                                            <li>Сертификация продукции для европейского рынка (СЕ)</li>
                                            <li>Получение документов в Морском регистре</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>Сертификация продукции для европейского рынка (СЕ)</li>
                            <li>Получение документов в Морском регистре</li>
                        </ul>
                        <section class="callback-primary-block bg-gradient-primary mb-4 mb-md-7 mx-md-n4 px-4">
                            <form class="row">
                                <div class="col-12 align-self-center">
                                    <h2 class="text-center">
                                        Запросить <span class="text-secondary">бесплатную</span> консультацию
                                    </h2>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="input">
                                        <label for="callbackPhone">Куда перезвонить?</label>
                                        <input placeholder="Телефон" type="tel" id="callbackPhone" data-maskphone>
                                    </div>
                                    <div class="input">
                                        <label for="callbackEmail">Что писать мы знаем, не знаем пока куда</label>
                                        <input placeholder="Электронная почта" type="email" id="callbackEmail">
                                    </div>
                                    <div class="input-cb input-cb_sm ml-lg-5 mb-md-0">
                                        <input type="checkbox" id="callbackTerms" checked>
                                        <label for="callbackTerms">
                                            Я принимаю условия <a href="#">оферты</a> и
                                            <a href="#">пользовательского
                                                соглашения</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="input">
                                        <label for="callbackMsg">Любые мысли по поводу и без. Вдруг станут
                                            классикой</label>
                                        <textarea placeholder="Комментарий" id="callbackMsg"
                                                  rows="3"></textarea>
                                    </div>
                                    <button class="btn btn_secondary px-0 w-100">Запросить бесплатно</button>
                                </div>
                            </form>
                        </section>
                        <ol>
                            <li>Сертификация ТР ТС</li>
                            <li>Сертификация продукции для европейского рынка (СЕ)
                                <ol>
                                    <li>Сертификация ТР ТС</li>
                                    <li>Сертификация продукции для европейского рынка (СЕ)</li>
                                    <li>Получение документов в Морском регистре</li>
                                </ol>
                            </li>
                            <li>Получение документов в Морском регистре</li>
                        </ol>
                        <table>
                            <thead>
                            <tr class="row h4">
                                <th class="col-4 text-left" scope="col">Наименование</th>
                                <th class="col-4 text-center" scope="col">Колонка 2</th>
                                <th class="col-4 text-right" scope="col">Колонка 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="row">
                                <td class="col-4 text-left ">Позиция 1</td>
                                <td class="col-4 text-center">Колонка 2</td>
                                <td class="col-4 text-right">1 000 000</td>
                            </tr>
                            <tr class="row">
                                <td class="col-4 text-left ">Позиция 2 <br> на 2 строки</td>
                                <td class="col-4 text-center">Колонка 2</td>
                                <td class="col-4 text-right">10 777 444</td>
                            </tr>
                            <tr class="row">
                                <td class="col-4 text-left">Позиция 3</td>
                                <td class="col-4 text-center">Колонка 2</td>
                                <td class="col-4 text-right">22.43</td>
                            </tr>
                            </tbody>
                        </table>
                    </article>
                    <div>
                        <hr class="mt-0 mb-4">
                        <div class="row mt-3">
                            <div class="col">
                                <h4 class="text-gray">Комментарии</h4>
                            </div>
                        </div>
                        <div class="input input_icon mb-4">
                            <i class="far fa-comment text-gray-light"></i>
                            <input type="text" placeholder="Напишите ваш комментарий"/>
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="comment">
                                    <div class="comment__body">
                                        <div class="comment__header">
                                            <a class="comment_author" href="#">
                                                <img class="comment__avatar"
                                                     src="/images/article/comment-img.png" alt="photo"> Иван
                                                Иванов
                                            </a>
                                            <div class="comment__datetime">
                                                <time datetime="2019-11-08T23:43" title="12.11.19 в 23:43">
                                                    12.11.19 в
                                                    23:43
                                                </time>
                                            </div>
                                        </div>
                                        <div class="comment__message">
                                            Задача организации, в особенности же высокое качество позиционных
                                            исследований требует от нас анализа кластеризации усилий! Ясность
                                            нашей
                                            позиции очевидна: высокотехнологичная концепция общественного уклада
                                            способствует повышению качества инновационных методов управления
                                            процессами.
                                        </div>
                                    </div>
                                    <button class="comment__answer-btn">Ответить <i
                                                class="fas fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-11 offset-1 col-md-9 offset-md-3">
                                <div class="comment">
                                    <div class="comment__body">
                                        <div class="comment__header">
                                            <a class="comment_author" href="#">
                                                <img class="comment__avatar"
                                                     src="/images/article/comment-img.png" alt="photo"> Иван
                                                Иванов
                                            </a>
                                            <div class="comment__datetime">
                                                <time datetime="2019-11-08T23:43" title="12.11.19 в 23:43">
                                                    12.11.19 в
                                                    23:43
                                                </time>
                                            </div>
                                        </div>
                                        <div class="comment__message">
                                            Задача организации, в особенности же высокое качество позиционных
                                            исследований требует от нас анализа кластеризации усилий! Ясность
                                            нашей
                                            позиции очевидна: высокотехнологичная концепция общественного уклада
                                            способствует повышению качества инновационных методов управления
                                            процессами.
                                        </div>
                                    </div>
                                    <button class="comment__answer-btn">Ответить <i
                                                class="fas fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <aside class="sidebar">
                        <div class="input input_icon input_sm input_bg-gradient">
                            <i class="fas fa-search text-primary"></i>
                            <input type="text" placeholder="Поиск">
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <ul class="nav tabs tabs_outline-blue w-100 flex-column flex-md-row">
                                    <li>
                                        <a class="tabs__item nav-link active" data-toggle="tab"
                                           href="#packageTab1">Популярные</a>
                                    </li>
                                    <li>
                                        <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab2">Рубрики</a>
                                    </li>
                                    <li>
                                        <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab3">Архивы</a>
                                    </li>
                                    <li>
                                        <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab4">Теги</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="packageTab1">
                                <div>
                                    <h4 class="mb-3">Популярные записи</h4>
                                    <div class="sidebar__anonce p-3 mb-5">
                                        <div class="article-row mb-3">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="article-row">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="packageTab2">
                                <div>
                                    <h4 class="mb-3">Рубрики</h4>
                                    <div class="sidebar__anonce p-3 mb-5">
                                        <div class="article-row mb-3">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="article-row">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="packageTab3">
                                <div>
                                    <h4 class="mb-3">Архивы</h4>
                                    <div class="sidebar__anonce p-3 mb-5">
                                        <div class="article-row mb-3">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="article-row">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="packageTab4">
                                <div>
                                    <h4 class="mb-3">Теги</h4>
                                    <div class="sidebar__anonce p-3 mb-5">
                                        <div class="article-row mb-3">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="article-row">
                                            <img class="article-row__image" src="/images/article/popular.jpg">
                                            <div class="article-row__text">
                                                <div class="text-primary">
                                                    <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                                </div>
                                                <h4 class="sidebar__anonce-link mt-2">
                                                    <a href="#" class="text-dark">Сертификация трансформаторов:
                                                        нормативные
                                                        акты,
                                                        необходимые документы...
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-6">
                            <h4 class="mb-3">Новые записи</h4>
                            <div class="article-row mb-3">
                                <img class="article-row__image" src="/images/article/popular.jpg">
                                <div class="article-row__text">
                                    <div class="text-gray-light">
                                        <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                    </div>
                                    <h4 class="sidebar__anonce-link mt-2">
                                        <a href="#" class="text-dark">Сертификация трансформаторов: нормативные акты,
                                            необходимые документы...
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <div class="article-row mb-3">
                                <img class="article-row__image" src="/images/article/popular.jpg">
                                <div class="article-row__text">
                                    <div class="text-gray-light">
                                        <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                                    </div>
                                    <h4 class="sidebar__anonce-link mt-2">
                                        <a href="#" class="text-dark">Сертификация трансформаторов: нормативные акты,
                                            необходимые документы...
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="mb-5">
                            <h4 class="mb-3">Свежие комментарии</h4>
                            <div class="sidebar__comments mb-4 font-weight-medium">
                                <div class="mb-3">
                                    <span class="text-gray">Человек Паук</span> прокоментировал запись <br>
                                    <a class="text-primary-light"
                                       href="#">«Российский морской регистр судоходства»</a>
                                </div>
                                <div class="mb-3">
                                    <span class="text-primary">Дед Пул</span> прокоментировал запись <br> <a
                                            class="text-primary-light"
                                            href="#">«Российский морской регистр судоходства»</a>
                                </div>
                                <div class="mb-3">
                                    <span class="text-gray">Человек Паук</span> прокоментировал запись <br>
                                    <a class="text-primary-light"
                                       href="#">«Российский морской регистр судоходства»</a>
                                </div>
                                <div>
                                    <span class="text-primary">Дед Пул</span> прокоментировал запись <br> <a
                                            class="text-primary-light"
                                            href="#">«Российский морской регистр судоходства»</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h4 class="mb-3">Рубрики</h4>
                            <div class="tab-content__rubrics mb-4">
                                <h4 class="mb-0">
                                    <a href="#" class="btn-rubrics mb-4">Добровольная сертификация</a>
                                </h4>
                                <h4 class="mb-0">
                                    <a href="#" class="btn-rubrics mb-4">Негосударственная экспертиза</a>
                                </h4>
                                <h4 class="mb-0">
                                    <a href="#" class="btn-rubrics mb-4">Подтверждение соответствия</a>
                                </h4>
                                <h4 class="mb-0">
                                    <a href="#" class="btn-rubrics mb-4">Сертификация экспорта</a>
                                </h4>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    @include('components.feedback')
@endsection
