@extends('layout')
@push('head')
    <title>Проекты</title>
@endpush
@section('content')
    <section class="entry-block entry-block_sm projects__entry-block">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-12 col-md-5 text-uppercase">
                    <h1 class="h2 mb-3">
                        Реализованные проекты.
                    </h1>
                    <h4 class="font-weight-normal">интересный текст —
                        исключительно интересный и познавательный текст
                    </h4>
                </div>
                <div class="col-12 col-md-7 text-center d-none d-md-flex position-static">
                    <img class="projects__entry-block-img" src="/images/projects/entry-man.png">
                </div>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Проекты'=>'/projects',
    ]])
    <section class="container page-block">
        <div class="row align-items-start mb-7">
            <div class="col-12 col-lg-6">
                <article class="article-card mb-6">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-2.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date">
                            <time datetime="2019-11-08" title="08.11.2019">08.11.2019</time>
                        </div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты действуют в отношении различных видов трансформаторов? Как получить
                        сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты компании...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-6">
                <article class="article-card mb-6">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-3.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация кондиционеров: как подтверждается соответствие сплит систем?</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты в области сертификации кондиционеров актуальны? Как получить сертификат
                        соответствия на кондиционер? Какие документы необходимо собрать для подачи ...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4">
                <article class="article-card mb-6">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-1.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Обучение по охране труда: от безопасности сотрудников к соблюдению закона</a>
                    </h3>
                    <p class="article-card__text">
                        Обучение по охране труда призвано уберечь персонал от несчастных случаев на рабочем месте,
                        благодаря подробному инструктированию с последующей проверкой полученных знаний. ...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4">
                <article class="article-card mb-6">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-2.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты действуют в отношении различных видов трансформаторов? Как получить
                        сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты компании...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4">
                <article class="article-card mb-6">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-3.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация кондиционеров: как подтверждается соответствие сплит систем?</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты в области сертификации кондиционеров актуальны? Как получить сертификат
                        соответствия на кондиционер? Какие документы необходимо собрать для подачи ...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4">
                <article class="article-card mb-6 mb-md-0">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-1.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Обучение по охране труда: от безопасности сотрудников к соблюдению закона</a>
                    </h3>
                    <p class="article-card__text">
                        Обучение по охране труда призвано уберечь персонал от несчастных случаев на рабочем месте,
                        благодаря подробному инструктированию с последующей проверкой полученных знаний. ...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4">
                <article class="article-card mb-6 mb-md-0">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-2.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация кондиционеров: как подтверждается соответствие сплит систем?</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты действуют в отношении различных видов трансформаторов? Как получить
                        сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты компании...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4">
                <article class="article-card mb-0">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-3.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date"><time datetime="2019-11-08" title="08.11.2019">08.11.2019</time></div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация кондиционеров: как подтверждается соответствие сплит систем?</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты в области сертификации кондиционеров актуальны? Как получить сертификат
                        соответствия на кондиционер? Какие документы необходимо собрать для подачи ...
                    </p>
                </article>
            </div>
        </div>
        <ul class="pagination">
            <li>
                <a class="pagination__item pagination__item_prev" href="#"></a>
            </li>
            <li>
                <a class="pagination__item" href="#">1</a>
            </li>
            <li>
                <a class="pagination__item" href="#">2</a>
            </li>
            <li>
                <a class="pagination__item pagination__item_current">3</a>
            </li>
            <li>
                <a class="pagination__item" href="#">4</a>
            </li>
            <li>
                <a class="pagination__item pagination__item_next" href="#"></a>
            </li>
        </ul>
    </section>
    @include('components.callback-primary')
    @include('components.director')
    <div class="mb-7"></div>
    @include('components.promo-cert')
    @include('components.feedback')
@endsection