@extends('layout')
@push('head')
    <title>О компании</title>
@endpush
@section('content')
    <div class="entry-block entry-block_sm about__entry-block overflow-hidden">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-12 col-md-6 text-uppercase">
                    <h1 class="h2 mb-3 mr-md-n5">
                        О КОМПАНИИ «ЛенТехСертификация».
                    </h1>
                    <p class="h4 font-weight-normal mr-n2">МЫ СОБРАЛИ САМЫЕ ЧАСТЫЕ
                        ВОПРОСЫ&nbsp;НАШИХ КЛИЕНТОВ В ОДНОМ МЕСТЕ
                    </p>
                </div>
                <div class="col-12 col-md-6 text-center d-none d-md-flex position-static">
                    <img class="" src="/images/faq/entry-girl.png">
                </div>
            </div>
        </div>
    </div>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'О компании'=>'/about',
    ]])
    @include('components.your-safe-partner')
    @include('components.our-directions')
    @include('components.company-principles')
    @include('components.why-we')
    @include('components.our-team')
    @include('components.certificates')
    @include('components.callback-primary')
    @include('components.feedback')
@endsection
