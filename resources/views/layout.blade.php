<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/vendor.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    @stack('head')
</head>
<body>
@include('components.navbar')
@yield('content')
@include('components.stay-modal')
@include('components.check-price-modal')
@include('components.footer')
<script src="/js/libs/polyfill.js"></script>
<script src="/js/libs/bootstrap-native-v4.js"></script>
<script src="/js/libs/jquery.min.js"></script>
<script src="/js/libs/jquery.fancybox.min.js"></script>
<script src="/js/libs/swiper.js"></script>
{{--<script src="/js/libs/imask.js"></script>--}}
<script src="/js/app.js"></script>
@stack('scripts')
</body>
</html>
