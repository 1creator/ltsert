@extends('layout')
@push('head')
    <title>Услуги</title>
@endpush
@section('content')
    <section class="position-relative z-11">
        <div class="swiper-container services__entry-swiper-container"
             data-prev="#entrySliderPrev"
             data-next="#entrySliderNext"
        >
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="entry-block services__entry-block">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                                    <h5 class="mb-6">Предлагаем провести испытания и оформить сертификаты морского и
                                        речного регистра судоходства
                                    </h5>
                                    <div class="row">
                                        <div class="col-12 col-md-10">
                                            <a href="#" class="btn btn_secondary w-100">Перейти в раздел</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 text-center d-none d-md-flex">
                                    <img class="services__entry-block-img" src="/images/services/sailor.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="entry-block services__entry-block">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                                    <h5 class="mb-6">Предлагаем провести испытания и оформить сертификаты морского и
                                        речного регистра судоходства
                                    </h5>
                                    <div class="row">
                                        <div class="col-12 col-md-10">
                                            <a href="#" class="btn btn_secondary w-100">Перейти в раздел</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 text-center d-none d-md-flex">
                                    <img class="services__entry-block-img" src="/images/services/sailor.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="entry-block services__entry-block">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                                    <h5 class="mb-6">Предлагаем провести испытания и оформить сертификаты морского и
                                        речного регистра судоходства
                                    </h5>
                                    <div class="row">
                                        <div class="col-12 col-md-10">
                                            <a href="#" class="btn btn_secondary w-100">Перейти в раздел</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 text-center d-none d-md-flex">
                                    <img class="services__entry-block-img" src="/images/services/sailor.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container entry-block__slider-controls-container">
            <div class="slider-controls entry-block__slider-controls">
                <button class="btn btn_primary slider-controls__prev" id="entrySliderPrev"></button>
                <button class="btn btn_primary slider-controls__next" id="entrySliderNext"></button>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Услуги'=>'/services',
    ]])
    @include('components.services')
    @component('components.callback')
        @slot('h4')
            Не нашли в списке, то что нужно?
        @endslot
        @slot('h5')
            Без паники! Оставьте номер телефона, и засекайте время.<br>Решение уже близко.
        @endslot
    @endcomponent
    @include('components.object-selector')
    @include('components.callback-primary')
    @include('components.blog')
    @include('components.feedback')
@endsection