@extends('layout')
@push('head')
    <title>FAQ</title>
@endpush
@section('content')
    <section class="entry-block entry-block_sm faq__entry-block overflow-hidden">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-12 col-md-5 text-uppercase">
                    <h1 class="h2 mb-3 mr-md-n5">
                        ВОПРОСЫ и ОТВЕТЫ.
                    </h1>
                    <p class="h4 font-weight-normal mr-n2">МЫ СОБРАЛИ САМЫЕ ЧАСТЫЕ
                        ВОПРОСЫ НАШИХ КЛИЕНТОВ В ОДНОМ МЕСТЕ
                    </p>
                </div>
                <div class="col-12 col-md-7 text-center d-none d-md-flex position-static">
                    <img class="" src="/images/faq/entry-girl.png">
                </div>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Вопросы и ответы'=>'/faq',
    ]])
    <section class="page-block">
        <div class="container">
            <div class="row faq__list-wrap">
                <div class="col-12 col-md-6">
                    <ul class="faq__list">
                        <li>
                            <h2 class="mb-0">
                                <a href="#faq1" class="h2 faq__list-toggler" data-toggle="collapse">Для чего нужен
                                    данный
                                    сертификат?</a>
                            </h2>
                            <div class="collapse faq__list-collapse" id="faq1">
                                <p>Необходим компаниям, которые производят продукцию, используемую в морском
                                    пространстве,
                                    судоходстве и работе на морях...</p>
                                <a href="#">Читать далее <i class="fas fa-chevron-right"></i></a>
                            </div>
                            <hr class="separator ml-7">
                        </li>
                        <li>
                            <h2 class="mb-0">
                                <a href="#faq2" class="h2 faq__list-toggler" data-toggle="collapse">Для чего нужен
                                    данный
                                    сертификат?</a>
                            </h2>
                            <div class="collapse faq__list-collapse" id="faq2">
                                <p>Необходим компаниям, которые производят продукцию, используемую в морском
                                    пространстве,
                                    судоходстве и работе на морях...</p>
                                <a href="#">Читать далее <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-6">
                    <ul class="faq__list">
                        <li>
                            <h2 class="mb-0">
                                <a href="#faq3" class="h2 faq__list-toggler" data-toggle="collapse">Для чего нужен
                                    данный
                                    сертификат?</a>
                            </h2>
                            <div class="collapse faq__list-collapse" id="faq3">
                                <p>Необходим компаниям, которые производят продукцию, используемую в морском
                                    пространстве,
                                    судоходстве и работе на морях...</p>
                                <a href="#">Читать далее <i class="fas fa-chevron-right"></i></a>
                            </div>
                            <hr class="separator ml-7">
                        </li>
                        <li>
                            <h2 class="mb-0">
                                <a href="#faq4" class="h2 faq__list-toggler" data-toggle="collapse">Для чего нужен
                                    данный
                                    сертификат?</a>
                            </h2>
                            <div class="collapse faq__list-collapse" id="faq4">
                                <p>Необходим компаниям, которые производят продукцию, используемую в морском
                                    пространстве,
                                    судоходстве и работе на морях...</p>
                                <a href="#">Читать далее <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="pagination">
                <li>
                    <a class="pagination__item pagination__item_prev" href="#"></a>
                </li>
                <li>
                    <a class="pagination__item" href="#">1</a>
                </li>
                <li>
                    <a class="pagination__item" href="#">2</a>
                </li>
                <li>
                    <a class="pagination__item pagination__item_current">3</a>
                </li>
                <li>
                    <a class="pagination__item" href="#">4</a>
                </li>
                <li>
                    <a class="pagination__item pagination__item_next" href="#"></a>
                </li>
            </ul>
        </div>
    </section>
    @include('components.callback',['h4'=>'Остались вопросы?','h5'=>'Оставьте номер телефона, и получите бесплатную консультацию эксперта'])
    @include('components.callback-primary')
    @include('components.director')
    <div class="mb-7"></div>
    @include('components.promo-cert')
    @include('components.feedback')
@endsection