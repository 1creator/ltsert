@extends('layout')
@push('head')
    <title>Услуга - сертификация сварщиков</title>
@endpush
@section('content')
    <section class="entry-block entry-block_sm text-white"
         style="background-image: url('/images/article/svarshiki.jpg')"
    >
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 col-md-11">
                    <h1 class="h2 mb-3 text-uppercase">
                        Сертификация (аттестация) сварщиков и операторов сварочных установок.
                    </h1>
                    <p class="h4 text-uppercase font-weight-normal">
                        СЕРТИФИКАТ МЕЖДУНАРОДНОГО ОБРАЗЦА
                    </p>
                </div>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Сертификация (аттестация) сварщиков и операторов сварочных установок'=>'/article-2',
    ]])
    <section class="page-block container">
        <h2 class="text-center mb-md-6">Хотите варить, но нет корочки?</h2>
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <h3>Это не проблема!</h3>
                <p class="mb-0">Прежде всего, разбавленное изрядной долей эмпатии, рациональное мышление однозначно определяет
                    каждого
                    участника как способного принимать собственные решения касаемо инновационных методов управления
                    процессами.
                    Лишь явные признаки победы институционализации объявлены нарушающими общечеловеческие нормы этики и
                    морали.
                    Не следует, однако, забывать, что сплочённость команды профессионалов представляет собой интересный
                    эксперимент проверки дальнейших направлений развития.
                </p>
            </div>
        </div>
    </section>
    @include('components.certificates',['header'=>'Какие документы получите'])
    @include('components.callback-primary')
    @include('components.feedback')
@endsection
