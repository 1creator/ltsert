@extends('layout')
@push('head')
    <title>Контакты</title>
@endpush
@section('content')
    <section class="entry-block entry-block_sm contacts__entry-block">
        <div class="container h-100 overflow-hidden">
            <div class="row align-items-center h-100">
                <div class="col-12 col-md-4">
                    <h1 class="h2 mb-4">КОНТАКТЫ.</h1>
                    <h4 class="text-uppercase font-weight-normal">свяжитесь с нами
                        или&nbsp;посетите наш офис в Санкт-Петербурге
                    </h4>
                </div>
                <div class="col-12 col-md-8 d-none d-md-block">
                    <img src="/images/contacts/entry-girl.png">
                </div>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Контакты'=>'/contacts',
    ]])
    <section class="map-block">
        <div class="map-block__map" id="map"></div>
        <div class="map-block__text">
            <div class="container">
                <div class="text-primary h3 mb-5">Санкт-Петербург</div>
                <div class="mb-4 d-flex">
                    <i class="fas fa-map-marker-alt text-primary mr-3"></i>
                    <div>
                        <div class="mb-1">Адрес нашего офиса:</div>
                        <div class="h4 text-primary-light mb-0">
                            Санкт-Петербург, пр. Комендантский д. 2, лит А, пом. 12-Н
                        </div>
                    </div>
                </div>
                <div class="mb-4 d-flex">
                    <i class="fas fa-phone-square-alt text-primary mr-3"></i>
                    <div>
                        <div class="mb-3">Телефоны:</div>
                        <a href="tel:+78126029142" class="h4 text-primary-light">+7 (812) 602-91-42</a>
                    </div>
                </div>
                <div class="mb-4 d-flex">
                    <i class="fas fa-envelope text-primary mr-3"></i>
                    <div>
                        <div class="mb-3">E-mail:</div>
                        <a href="mailto:info@ltsert.ru" class="h4 text-primary-light">info@Ltsert.ru</a>
                    </div>
                </div>
                <div class="mb-4 d-flex">
                    <i class="far fa-clock text-primary mr-3"></i>
                    <div>
                        <div class="mb-3">Часы работы:</div>
                        <div class="d-flex flex-column flex-sm-row">
                            <div class="h4 text-primary-light mb-0 mr-5">ПН - ПТ: 09:00 — 19:00</div>
                            <div class="h4 contacts__weekends d-flex flex-column flex-sm-row">
                                <span class="mr-5">Сб: Выходной</span> <span>Вс: Выходной</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contacts__other-cities">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="text-primary h3 mb-5">Москва</div>
                    <div class="mb-4 d-flex">
                        <i class="fas fa-phone-square-alt text-primary mr-3"></i>
                        <div>
                            <div class="mb-1">Телефоны:</div>
                            <a href="tel:+74996498469" class="h4 text-primary-light">+7 (499) 649-84-69</a>
                        </div>
                    </div>
                    <div class="mb-4 mb-md-0 d-flex">
                        <i class="fas fa-envelope text-primary mr-3"></i>
                        <div>
                            <div class="mb-3">E-mail:</div>
                            <a href="mailto:info@ltsert.ru"
                               class="h4 text-primary-light">info@Ltsert.ru</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="text-primary h3 mb-5">Россия</div>
                    <div class="mb-4 d-flex">
                        <i class="fas fa-phone-square-alt text-primary mr-3"></i>
                        <div>
                            <div class="mb-1">Телефоны:</div>
                            <a href="tel:+78005056209" class="h4 text-secondary">8 800 505-62-09</a>
                        </div>
                    </div>
                    <div class="d-flex">
                        <i class="fas fa-envelope text-primary mr-3"></i>
                        <div>
                            <div class="mb-3">E-mail:</div>
                            <a href="mailto:info@ltsert.ru"
                               class="h4 text-primary-light">info@Ltsert.ru</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('components.callback-primary')
    @include('components.director')
    @include('components.feedback')
@endsection
@push('scripts')
    <script>
        let map;
        let center;
        if (window.outerWidth < 992) {
            center = {lat: 60.001031, lng: 30.265841};
        } else {
            center = {lat: 60.004082, lng: 30.045840};
        }


        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: center,
                zoom: 11,
            });

            new google.maps.Marker({
                position: {lat: 60.001031, lng: 30.265841},
                map: map,
                icon: '/images/map-marker.svg',
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkutDJ_NT_thZ67kNleQiElrV2Aw5XJgg&callback=initMap"
            async defer></script>
@endpush