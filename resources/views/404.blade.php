@extends('layout')
@push('head')
    <title>404 - Страница не найдена</title>
@endpush
@section('content')
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        '404'=>'',
    ]])
    <div class="entry-block p-404__entry-block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 pl-md-7 align-self-end ">
                    <img class="p-404__entry-block-img" src="/images/404/man.png">
                </div>
                <div class="col-12 col-md-4 offset-md-1">
                    <h3 class="text-primary mb-4">Капитан, возникла проблема.</h3>
                    <div class="h4 font-weight-normal mb-5">Я искал-искал, но такой страницы нет</div>
                    <a href="/" class="btn btn_primary w-100">На главную</a>
                </div>
            </div>
        </div>
    </div>
    @include('components.callback-primary')
@endsection