@extends('layout')
@push('head')
    <title>Проект</title>
@endpush
@section('content')
    <section class="entry-block entry-block_sm project__entry-block overflow-hidden">
        <div class="container h-100">
            <div class="row h-100 position-relative">
                <div class="col-12 col-md-5 align-self-center project__entry-block-text">
                    <h1 class="h2 mb-3">НАЗВАНИЕ
                        ПРОЕКТА.
                    </h1>
                    <p class="h4 text-uppercase font-weight-normal">
                        интересный текст —
                        исключительно интересный и познавательный текст
                    </p>
                </div>
                <div class="col-12 col-md-7 project__entry-block-img-wrap">
                    <img class="project__entry-block-img" src="/images/project/entry-bg.png">
                </div>
            </div>
            <div class="col-12 col-md-7 project__entry-block-img-wrap">
                <img class="project__entry-block-img" src="/images/project/entry-bg.png">
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Проекты'=>'/projects',
        'Реализованный проект'=>'/project',
    ]])
    <section class="page-block">
        <div class="container ">
            <div class="row client project__client align-items-baseline">
                <div class="col-md-2 text-gray">Клиент</div>
                <div class="col-md-8 d-flex align-items-baseline">
                    ООО <h3 class="mb-0 ml-3">«Море волнуется раз»</h3>
                </div>
                <div class="col-md-2 text-right"><a href="#">
                        <h4 class="mb-0 text-primary-light">www.mvr.ru</h4>
                    </a></div>
            </div>

            <div class="row">
                <div class="col-md-1">
                    <hr class="project__separator">
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 text-gray">Цели клиента</div>
                <div class="col-md-7">
                    <ul class="project__list">
                        <li class="h4 project__list-item">Цель 1. Описание цели номер один</li>
                        <li class="h4 project__list-item">Цель 2. Описание цели номер два</li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1">
                    <hr class="project__separator">
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 text-gray">Задачи ЛТС <br> Групп</div>
                <div class="col-md-7">
                    <ul class="project__list">
                        <li class="h4 project__list-item">Задача 1. Описание задачи номер один</li>
                        <li class="h4 project__list-item">Задача 2. Описание задачи номер два</li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1">
                    <hr class="project__separator">
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 text-gray">Реализация</div>
                <div class="col-md-9">
                    <p class="h4 font-weight-normal">Шаг 1</p>
                    <p>В некотором царстве, в некотором государстве жил да был царь с царицею, у него было три сына —
                        все молодые, холостые, удальцы такие, что ни в сказке сказать, ни пером написать; младшего звали
                        Иван-царевич.</p>

                    <p>Говорит им царь таково слово:</p>

                    <p>«Дети мои милые, возьмите себе по стрелке, натяните тугие луки и пустите в разные стороны; на чей
                        двор стрела упадет, там и сватайтесь».</p>

                    <p>Пустил стрелу старший брат — упала она на боярский двор, прямо против девичья терема; пустил
                        средний брат — полетела стрела к купцу на двор и остановилась у красного крыльца, а на там
                        крыльце стояла душа-девица, дочь купеческая, пустил младший брат — попала стрела в грязное
                        болото, и подхватила её лягуша-квакуша.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1">
                    <hr class="project__separator">
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 text-gray">Итог</div>
                <div class="col-md-9">
                    <p class="h4 font-weight-normal">Вывод 1</p>
                    <p>Иван-царевич пошел в дом Кощея, взял Василису Премудрую и воротился домой. После того они жили
                        вместе и долго и счастливо. Вот и сказке Царевна-лягушка конец, а кто слушал - молодец!</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1">
                    <hr class="project__separator">
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 text-gray">Отзыв клиента</div>
                <div class="col-md-10">
                    <blockquote class="review-card review-card_combined">
                        <a class="review-card__left" data-fancybox href="/images/project/review.jpg">
                            <div class="review-card__image"
                                 style="background-image: url('/images/project/review.jpg');">
                            </div>
                            <div class="review-card__gradient"></div>
                            <div class="review-card__date">08.11.2019</div>
                        </a>
                        <div class="review-card__right" style="width: 74%;">
                            <p class="review-card__right-text">За время работы «ЛенТехСертификация» зарекомендовала
                                себя
                                как компетентная, ответственная и исполнительная компания. Работы по сертификации
                                выполнялись качественно и в согласованные сроки.
                            </p>
                            <div class="review-card__stars">
                                <img src="/images/project/icon-star.svg">
                                <img src="/images/project/icon-star.svg">
                                <img src="/images/project/icon-star.svg">
                                <img src="/images/project/icon-star.svg">
                                <img src="/images/project/icon-star.svg">
                            </div>
                            <footer class="review-card__author">
                                <p class="mb-0"><b>Дядька Черномор,</b></p>
                                <cite class="font-normal">директор ООО «Море волнуется раз»</cite>
                            </footer>
                        </div>
                    </blockquote>
                </div>
            </div>
        </div>
    </section>
    @include('components.callback-primary')
    @include('components.cases')
    @include('components.director')
    @include('components.feedback')
@endsection
