@extends('layout')
@push('head')
    <title>Услуга</title>
@endpush
@section('content')
    <section class="entry-block service__entry-block bg-gradient-primary-light">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 align-self-center">
                    <h1 class="h2 text-primary-light">Сертификат Российского
                        морского и речного регистра судоходства
                    </h1>
                </div>
                <div class="col-12 col-md-4 text-center d-none d-md-flex position-static">
                    <img class="service__entry-block-img" src="/images/services/sailor.png">
                </div>
                <form class="col-12 col-md-4">
                    <div class="h4 mb-4 text-invert text-center">ЗАКАЗАТЬ СЕРТИФИКАТ</div>
                    <div class="input">
                        <label for="promoPhone">Куда перезвонить?</label>
                        <input placeholder="Телефон" type="tel" id="promoPhone" data-maskphone>
                    </div>
                    <div class="input">
                        <label for="promoEmail">Что писать мы знаем, не знаем пока куда</label>
                        <input placeholder="Электронная почта" type="email" id="promoEmail">
                    </div>
                    <div class="input mb-3">
                        <label for="promoMsg">Любые мысли по поводу и без. Вдруг станут классикой</label>
                        <textarea placeholder="Комментарий" id="promoMsg"></textarea>
                    </div>
                    <div class="input-cb input-cb_sm">
                        <input type="checkbox" id="promoTerms" checked>
                        <label class="pr-5" for="promoTerms">
                            Я принимаю условия <a href="#">оферты</a> и <a href="#">пользовательского соглашения</a>
                        </label>
                    </div>
                    <button type="submit" class="btn btn_primary px-0 w-100">Получить&nbsp;подарок</button>
                </form>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Услуги'=>'/services',
        'Сертифика т российского морского и речного регистра'=>'/service',
    ]])
    <section class="container page-block">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-1 pt-6">
                <h2 class="pr-md-6 mb-6">Для чего нужен данный сертификат?</h2>
                <hr class="separator mb-6">
                <p class="pr-7">Необходим компаниям, которые производят продукцию, используемую в морском пространстве,
                    судоходстве и
                    работе на морях.</p>
            </div>
            <div class="col-12 col-md-4">
                <img class="service__cert-image" src="/images/service/certificate.png">
            </div>
        </div>
    </section>
    <section class="container">
        <div class="col-12 col-md-10 offset-md-1">
            <h2 class="text-center mb-md-6">На какую продукцию необходим:</h2>
            <p class="text-center text-primary-light mb-md-5">Морским регистром осуществляется техническое наблюдение за
                изготовлением материалов и изделий в соответствии
                с требованиями правил РС, международных конвенций и соглашений, рекомендаций Международной морской
                организации и морских администраций.</p>
            <ul class="cell-list">
                <li><h4 class="mb-0"><a href="#" class="text-primary-light">Спасательные средства</a></h4></li>
                <li><h4 class="mb-0"><a href="#" class="text-primary-light">Корпусные конструкции</a></h4></li>
                <li><h4 class="mb-0"><a href="#" class="text-primary-light">Оборудование, снабжение</a></h4></li>
                <li><h4 class="mb-0"><a href="#" class="text-primary-light">Сигнальные средства</a></h4></li>
                <li><h4 class="mb-0"><a href="#" class="text-primary-light">Радиооборудование</a></h4></li>
                <li><h4 class="mb-0"><a href="#" class="text-primary-light">Противопожарная защита</a></h4></li>
            </ul>
        </div>
    </section>
    @component('components.callback')
        @slot('h4')
            Не уверены, что именно этот сертификат вам нужен?
        @endslot
        @slot('h5')
            Без паники! Оставьте номер телефона, и засекайте время.<br>Решение уже близко.
        @endslot
    @endcomponent
    <section class="certificates-block bg-gray-light-ultra">
        <div class="page-block container pb-6">
            <h2 class="text-center mb-md-6">
                Какие документы будут получены
            </h2>
            <div class="row mb-5">
                <div class="col-12 col-md-10 offset-md-1 text-center text-primary-light px-md-5">
                    Морским регистром осуществляется техническое наблюдение за изготовлением материалов и изделий в
                    соответствии
                    с требованиями правил РС, международных конвенций и соглашений, рекомендаций Международной морской
                    организации и морских администраций.
                </div>
            </div>
            <div class="swiper-container certificates-block__swiper-container my-n7 py-7"
                 data-prev="#certificatesPrev"
                 data-next="#certificatesNext"
                 data-spacebetween="1000"
            >
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row align-items-center ">
                            <div class="col-12 col-md-4 offset-md-1 mb-3 mb-md-0">
                                <img src="/images/index/certificate.png" class="certificates-block__img mw-100">
                            </div>
                            <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                                <h3 class="text-primary-light mb-4">Свидетельство о соответствии предприятия</h3>
                                <p>Данное свидетельство позволяет на оформлять</p>
                                <p>то</p>
                                <p>это</p>
                                <p>и еще вот это</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row align-items-center ">
                            <div class="col-12 col-md-4 offset-md-1 mb-3 mb-md-0">
                                <img src="/images/index/certificate.png" class="certificates-block__img mw-100">
                            </div>
                            <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                                <h3 class="text-primary-light mb-4">Свидетельство о соответствии предприятия</h3>
                                <p>Данное свидетельство позволяет на оформлять</p>
                                <p>то</p>
                                <p>это</p>
                                <p>и еще вот это</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row align-items-center ">
                            <div class="col-12 col-md-4 offset-md-1 mb-3 mb-md-0">
                                <img src="/images/index/certificate.png" class="certificates-block__img mw-100">
                            </div>
                            <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                                <h3 class="text-primary-light mb-4">Свидетельство о соответствии предприятия</h3>
                                <p>Данное свидетельство позволяет на оформлять</p>
                                <p>то</p>
                                <p>это</p>
                                <p>и еще вот это</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-3 offset-md-6">
                        <div class="slider-controls certificates-block__slider-controls">
                            <button class="btn btn_primary slider-controls__prev" id="certificatesPrev"></button>
                            <button class="btn btn_primary slider-controls__next" id="certificatesNext"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="certificates-block bg-white">
        <div class="page-block container">
            <h2 class="text-center mb-md-6">
                Какие документы нужно подготовить
            </h2>
            <div class="row mb-5">
                <div class="col-12 col-md-10 offset-md-1 text-center text-primary-light px-md-5">
                    Морским регистром осуществляется техническое наблюдение за изготовлением материалов и изделий в
                    соответствии
                    с требованиями правил РС, международных конвенций и соглашений, рекомендаций Международной морской
                    организации и морских администраций.
                </div>
            </div>
            <div class="swiper-container certificates-block__swiper-container"
                 data-prev="#certificates2Prev"
                 data-next="#certificates2Next"
                 data-spacebetween="1000"
            >
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row align-items-center ">
                            <div class="col-12 col-md-4 offset-md-1 mb-3 mb-md-0">
                                <img src="/images/index/certificate.png" class="certificates-block__img mw-100">
                            </div>
                            <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                                <h3 class="text-primary-light mb-4">Свидетельство о соответствии предприятия</h3>
                                <p>Данное свидетельство позволяет на оформлять</p>
                                <p>то</p>
                                <p>это</p>
                                <p>и еще вот это</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row align-items-center ">
                            <div class="col-12 col-md-4 offset-md-1 mb-3 mb-md-0">
                                <img src="/images/index/certificate.png" class="certificates-block__img mw-100">
                            </div>
                            <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                                <h3 class="text-primary-light mb-4">Свидетельство о соответствии предприятия</h3>
                                <p>Данное свидетельство позволяет на оформлять</p>
                                <p>то</p>
                                <p>это</p>
                                <p>и еще вот это</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row align-items-center ">
                            <div class="col-12 col-md-4 offset-md-1 mb-3 mb-md-0">
                                <img src="/images/index/certificate.png" class="certificates-block__img mw-100">
                            </div>
                            <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                                <h3 class="ext-primary-light mb-4">Свидетельство о соответствии предприятия</h3>
                                <p>Данное свидетельство позволяет на оформлять</p>
                                <p>то</p>
                                <p>это</p>
                                <p>и еще вот это</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-3 offset-md-6">
                        <div class="slider-controls certificates-block__slider-controls">
                            <button class="btn btn_primary slider-controls__prev" id="certificates2Prev"></button>
                            <button class="btn btn_primary slider-controls__next" id="certificates2Next"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="page-block service__price-block bg-gradient">
        <div class="container">
            <h2 class="mb-md-6 text-center">Стоимость и сроки</h2>
            <div class="table-responsive">
                <table class="table service__price-table">
                    <tbody>
                    <tr class="row service__price-table-row">
                        <td class="col-6 offset-md-1 service__price-table-col-1">Название услуги номер 1</td>
                        <td class="col-2 text-center">9 раб. дней</td>
                        <td class="col-2 text-center">200 000 руб.</td>
                    </tr>
                    <tr class="row service__price-table-row">
                        <td class="col-6 offset-md-1 service__price-table-col-1">Название услуги номер 2</td>
                        <td class="col-2 text-center">60 раб. дней</td>
                        <td class="col-2 text-center">600 000 руб.</td>
                    </tr>
                    <tr class="row service__price-table-row">
                        <td class="col-6 offset-md-1 service__price-table-col-1">Название услуги номер 3</td>
                        <td class="col-2 text-center">9 раб. дней</td>
                        <td class="col-2 text-center">200 000 руб.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <section class="packages-block page-block bg-gradient-secondary">
        <div class="container">
            <h2 class="text-white text-center mb-md-6">Готовые пакеты</h2>
            <div class="row mb-5">
                <div class="col-12 col-md-6 offset-md-3 px-5">
                    <ul class="nav tabs tabs_outline w-100">
                        <li>
                            <a class="tabs__item nav-link active" data-toggle="tab" href="#packageTab1">ВИД 1</a>
                        </li>
                        <li>
                            <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab2">ВИД 1</a>
                        </li>
                        <li>
                            <a class="tabs__item nav-link" data-toggle="tab" href="#packageTab3">ВИД 2</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="packageTab1">
                    <div class="row">
                        <div class="col-12 col-md-4 mb-4 mb-md-0">
                            <div class="package-card mb-2">
                                <h3 class="package-card__title">
                                    МИНИМУМ
                                </h3>
                                <ul class="package-card__items">
                                    <li class="package-card__item">Сбор первичной документации</li>
                                    <li class="package-card__item">Анализ документации</li>
                                    <li class="package-card__item">Построение алгоритма работы с РМРС</li>
                                    <li class="package-card__item">Написание отчета по анализу технической документации
                                        на соответствия правил РМРС
                                    </li>
                                    <li class="package-card__item">Доработка тех. Условий согласно правилам РМРС</li>
                                    <li class="package-card__item">Доработка Программы методики испытаний<br/>
                                        согласно правилам РМРС
                                    </li>
                                    <li class="package-card__item">Формирование папки документов согласно правилам
                                        РМРС
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Подача и согласование
                                        первичной документации в РМРС
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Организация испытаний
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Проведение испытаний</li>
                                    <li class="package-card__item package-card__item_disabled mx-n2">Выезд нашего
                                        эксперта для
                                        проведения и подготовки организации заказчика перед приездом инспектора РМРС
                                    </li>
                                    <li class="package-card__item package-card__item_disabled mx-n2">Организация выезда
                                        инспектора РМРС
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Получение Акта
                                        освидетельствования
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Окончательное
                                        согласование пакета документов в РМРС
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Получение СТО</li>
                                    <li class="package-card__item package-card__item_disabled">Общение с РМРС с участием
                                        заказчика
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Общение с РМРС на прямую,
                                        без участия заказчика
                                    </li>
                                </ul>
                                <div class="package-card__price">~ дней / 480 000 руб.</div>
                            </div>
                            <button class="btn btn_secondary w-100">Приобрести</button>
                        </div>
                        <div class="col-12 col-md-4 mb-4 mb-md-0">
                            <div class="package-card mb-2">
                                <h3 class="package-card__title text-secondary">
                                    СТАНДАРТ
                                </h3>
                                <ul class="package-card__items">
                                    <li class="package-card__item">Сбор первичной документации</li>
                                    <li class="package-card__item">Анализ документации</li>
                                    <li class="package-card__item">Построение алгоритма работы с РМРС</li>
                                    <li class="package-card__item package-card__item_disabled">Написание отчета по
                                        анализу технической документации
                                        на соответствия правил РМРС
                                    </li>
                                    <li class="package-card__item">Доработка тех. Условий согласно правилам РМРС</li>
                                    <li class="package-card__item">Доработка Программы методики испытаний<br/>
                                        согласно правилам РМРС
                                    </li>
                                    <li class="package-card__item">Формирование папки документов согласно правилам
                                        РМРС
                                    </li>
                                    <li class="package-card__item">Подача и согласование
                                        первичной документации в РМРС
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Организация испытаний
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Проведение испытаний</li>
                                    <li class="package-card__item package-card__item_disabled mx-n2">Выезд нашего
                                        эксперта для
                                        проведения и подготовки организации заказчика перед приездом инспектора РМРС
                                    </li>
                                    <li class="package-card__item mx-n2">Организация выезда
                                        инспектора РМРС
                                    </li>
                                    <li class="package-card__item">Получение Акта
                                        освидетельствования
                                    </li>
                                    <li class="package-card__item">Окончательное
                                        согласование пакета документов в РМРС
                                    </li>
                                    <li class="package-card__item">Получение СТО</li>
                                    <li class="package-card__item">Общение с РМРС с участием
                                        заказчика
                                    </li>
                                    <li class="package-card__item package-card__item_disabled">Общение с РМРС на прямую,
                                        без участия заказчика
                                    </li>
                                </ul>
                                <div class="package-card__price">120 дней / 600 000 руб.</div>
                            </div>
                            <button class="btn btn_secondary w-100">Приобрести</button>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="package-card mb-2">
                                <h3 class="package-card__title">
                                    ПОД КЛЮЧ
                                </h3>
                                <ul class="package-card__items">
                                    <li class="package-card__item">Сбор первичной документации</li>
                                    <li class="package-card__item">Анализ документации</li>
                                    <li class="package-card__item">Построение алгоритма работы с РМРС</li>
                                    <li class="package-card__item package-card__item_disabled">Написание отчета по
                                        анализу технической документации
                                        на соответствия правил РМРС
                                    </li>
                                    <li class="package-card__item">Доработка тех. Условий согласно правилам РМРС</li>
                                    <li class="package-card__item">Доработка Программы методики испытаний<br/>
                                        согласно правилам РМРС
                                    </li>
                                    <li class="package-card__item">Формирование папки документов согласно правилам
                                        РМРС
                                    </li>
                                    <li class="package-card__item">Подача и согласование
                                        первичной документации в РМРС
                                    </li>
                                    <li class="package-card__item">Организация испытаний
                                    </li>
                                    <li class="package-card__item">Проведение испытаний</li>
                                    <li class="package-card__item mx-n2">Выезд нашего
                                        эксперта для
                                        проведения и подготовки организации заказчика перед приездом инспектора РМРС
                                    </li>
                                    <li class="package-card__item mx-n2">Организация выезда
                                        инспектора РМРС
                                    </li>
                                    <li class="package-card__item">Получение Акта
                                        освидетельствования
                                    </li>
                                    <li class="package-card__item">Окончательное
                                        согласование пакета документов в РМРС
                                    </li>
                                    <li class="package-card__item">Получение СТО</li>
                                    <li class="package-card__item package-card__item_disabled">Общение с РМРС с участием
                                        заказчика
                                    </li>
                                    <li class="package-card__item">Общение с РМРС на прямую,
                                        без участия заказчика
                                    </li>
                                </ul>
                                <div class="package-card__price">90 дней / 840 000 руб.</div>
                            </div>
                            <button class="btn btn_secondary w-100">Приобрести</button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane container fade" id="packageTab2">2</div>
                <div class="tab-pane container fade" id="packageTab3">3</div>
            </div>
        </div>
    </section>
    <section class="page-block">
        <div class="container">
            <div class="video-card">
                <iframe class="video-card__iframe"
                        src="https://www.youtube.com/embed/kvDwwHIugqg?enablejsapi=1" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                <div class="video-card__foreground">
                    <button class="fas fa-play-circle video-card__play-btn"></button>
                </div>
            </div>
        </div>
    </section>
    @include('components.certificates')
    @include('components.promo-cert')
    @include('components.reviews')
    <section class="service-related-products-block page-block bg-gradient">
        <div class="container">
            <h2 class="text-center mb-md-6">Вместе с сертификатом также заказывают</h2>
            <div class="row">
                <div class="col-12 col-md-4 mb-3 mb-md-0">
                    <div class="related-product-card">
                        <h4 class="mb-4">
                            <a href="#" class="text-primary-light">Сертификат соответствия Таможеного союза</a>
                        </h4>
                        <p class="mb-0">Необходим для ввоза и реализации продукции на территории всех стран Таможенного
                            союза.
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3 mb-md-0">
                    <div class="related-product-card">
                        <h4 class="mb-4">
                            <a href="#" class="text-primary-light">Сертификат соответствия Таможеного союза</a></h4>
                        <p class="mb-0">Необходим для ввоза и реализации продукции на территории всех стран Таможенного
                            союза.
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="related-product-card">
                        <h4 class="mb-4">
                            <a href="#" class="text-primary-light">Сертификат соответствия Таможеного союза</a>
                        </h4>
                        <p class="mb-0">Необходим для ввоза и реализации продукции на территории всех стран Таможенного
                            союза.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('components.cases')
    @include('components.callback-primary')
    @include('components.feedback')
@endsection

@push('scripts')
    <script>
        document.getElementsByClassName('video-card__play-btn')[0]
            .addEventListener('click', function () {
                let iframe = this.parentElement.parentElement.children[0];
                iframe.contentWindow.postMessage('{"event": "command", "func": "playVideo", "args": ""}', "*");
                // iframe.src = iframe.src + '?autoplay=1';
                this.parentElement.remove();
            });
    </script>
@endpush