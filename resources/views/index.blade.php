@extends('layout')
@push('head')
    <title>Главная</title>
@endpush
@section('content')
    <section class="position-relative">
        <div class="swiper-container"
             data-prev="#entrySliderPrev"
             data-next="#entrySliderNext"
        >
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="entry-block index__entry-block">
                        <div class="container position-relative">
                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                                    <h5 class="mb-6">Предлагаем провести испытания и оформить сертификаты морского и
                                        речного регистра судоходства
                                    </h5>
                                    <div class="row">
                                        <div class="col-12 col-md-10">
                                            <a href="#stayModal" class="btn btn_secondary w-100"
                                               data-toggle="modal">Перейти в раздел</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 text-center d-none d-md-block">
                                    <a data-fancybox href="/images/index/certificate.png">
                                        <img class="index__entry-block-img" src="/images/index/certificate.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="entry-block index__entry-block">
                        <div class="container position-relative">
                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                                    <h5 class="mb-6">Предлагаем провести испытания и оформить сертификаты морского и
                                        речного регистра судоходства
                                    </h5>
                                    <div class="row">
                                        <div class="col-12 col-md-10">
                                            <a href="#stayModal" class="btn btn_secondary w-100"
                                               data-toggle="modal">Перейти в раздел</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 text-center d-none d-md-block">
                                    <a data-fancybox href="/images/index/certificate.png">
                                        <img class="index__entry-block-img" src="/images/index/certificate.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container entry-block__slider-controls-container">
            <div class="slider-controls entry-block__slider-controls">
                <button class="btn btn_primary slider-controls__prev" id="entrySliderPrev"></button>
                <button class="btn btn_primary slider-controls__next" id="entrySliderNext"></button>
            </div>
        </div>
    </section>
    @include('components.services')
    @component('components.callback')
        @slot('h5')
            Без паники! Оставьте номер телефона, и засекайте время.<br>Решение уже близко.
        @endslot
    @endcomponent
    @include('components.promo-cert')
    @include('components.advantages')
    <div class="mt-n6"></div>
    @component('components.callback')
        @slot('h4')
            «Все звучит очень серьезно, но мне нужно подумать».
        @endslot
        @slot('h5')
            Если, вдруг, вы так думаете, позвольте позвонить вам и<br/>развеять ваши опасения.
        @endslot
    @endcomponent
    @include('components.object-selector')
    @include('components.certificates')
    @include('components.callback-primary')
    @include('components.director')
    @include('components.clients')
    @include('components.blog')
    @include('components.feedback')
@endsection
