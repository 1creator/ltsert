<section class="page-block object-selector bg-gradient">
    <form class="container">
        <h2 class="text-center mb-6">ВЫБЕРИТЕ ОБЪЕКТ СЕРТИФИКАЦИИ</h2>
        <div class="object-selector__nav-wrap mb-6">
            <ul class="object-selector__nav">
                <li><a href="#" class="object-selector__nav-item object-selector__nav-item_active">А</a></li>
                <li><a href="#" class="object-selector__nav-item">Б</a></li>
                <li><a href="#" class="object-selector__nav-item">В</a></li>
                <li><a href="#" class="object-selector__nav-item">Г</a></li>
                <li><a href="#" class="object-selector__nav-item">Д</a></li>
                <li><a href="#" class="object-selector__nav-item">Е</a></li>
                <li><a href="#" class="object-selector__nav-item object-selector__nav-item_disabled">Ё</a></li>
                <li><a href="#" class="object-selector__nav-item">Ж</a></li>
                <li><a href="#" class="object-selector__nav-item">З</a></li>
                <li><a href="#" class="object-selector__nav-item">И</a></li>
                <li><a href="#" class="object-selector__nav-item">К</a></li>
                <li><a href="#" class="object-selector__nav-item">Л</a></li>
                <li><a href="#" class="object-selector__nav-item">М</a></li>
                <li><a href="#" class="object-selector__nav-item">Н</a></li>
                <li><a href="#" class="object-selector__nav-item">О</a></li>
                <li><a href="#" class="object-selector__nav-item">П</a></li>
                <li><a href="#" class="object-selector__nav-item">Р</a></li>
                <li><a href="#" class="object-selector__nav-item">С</a></li>
                <li><a href="#" class="object-selector__nav-item">Т</a></li>
                <li><a href="#" class="object-selector__nav-item">У</a></li>
                <li><a href="#" class="object-selector__nav-item">Ф</a></li>
                <li><a href="#" class="object-selector__nav-item">Х</a></li>
                <li><a href="#" class="object-selector__nav-item">Ц</a></li>
                <li><a href="#" class="object-selector__nav-item">Ч</a></li>
                <li><a href="#" class="object-selector__nav-item">Ш</a></li>
                <li><a href="#" class="object-selector__nav-item">Щ</a></li>
                <li><a href="#" class="object-selector__nav-item object-selector__nav-item_disabled">Ы</a></li>
                <li><a href="#" class="object-selector__nav-item">Э</a></li>
                <li><a href="#" class="object-selector__nav-item object-selector__nav-item_disabled">Ю</a></li>
                <li><a href="#" class="object-selector__nav-item">Я</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="input input_btn mb-6">
                    <label for="objectSelectorSearch"></label>
                    <input placeholder="Что ищем?" type="text" id="objectSelectorSearch">
                    <button class="btn btn_primary" type="submit">
                        <span class="d-none d-md-block">Поиск</span>
                        <i class="fa fas fa-search px-4 d-md-none"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="object-selector__content">
            <div class="row mb-5">
                <div class="col-12 col-md-4 pr-md-5">
                    <h4 class="mb-3"><a href="#">Автоматические выключатели</a></h4>
                    <p>Согласно требованиям принятых в ЕАЭС технических регламентов, автоматические выключатели подлежат
                        обязательной оценке соответствия. Следовательно, вам для осуществления производства, импорта и
                        распространения данного оборудования нужно обратиться в аккредитованный центр и оформить
                        обязательный сертификат соответствия на автоматические выключатели.</p>
                    <p>
                        Этот документ подтверждает тот факт, что заявленное оборудование безопасно и качественно, так
                        как
                        оно полностью соответст...
                    </p>
                </div>
                <div class="col-12 col-md-4 pr-md-5">
                    <h4 class="mb-3"><a href="#">Автомобильные запчасти</a></h4>
                    <p>От того, насколько качественно изготовлены автомобильные запчасти, может зависеть жизнь как
                        пассажиров и водителя автомобиля, так и других участников дорожного движения. Поэтому
                        сертификация
                        автомобильных запчастей столь важный и значимый процесс.</p>
                    <p>

                        Экономическом Союзе действует технический регламент на колесные транспортные средства, под
                        действие
                        которого попадают и автомобильные запчасти. Таким образом, сертификация автомобильных запчастей
                        производится на основании профильного техрегламента и, соответственно, является...
                    </p>
                </div>
                <div class="col-12 col-md-4 pr-md-5">
                    <h4 class="mb-3"><a href="#">Автосервис</a></h4>
                    <p>Отечественные предприниматели, оказывающие услуги автосервиса, зачастую проходят оценку
                        соответствия
                        своей деятельности. Проводится сертификация автосервиса в рамках специальной системы
                        подтверждения
                        качества. В ходе такой процедуры осуществляются определенные проверки и экспертизы: проверяется
                        оснащенность объекта необходимым оборудованием, анализируется квалификация персонала и другое. В
                        случае соответствия автосервиса установленным в его отношении требованиям предпринимателю
                        выдается
                        сертификат...</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4 offset-md-4">
                    <a href="#" class="btn btn_primary w-100">Весь список</a>
                </div>
            </div>
        </div>
    </form>
</section>