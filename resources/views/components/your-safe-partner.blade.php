<section class="container page-block">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center mb-6">Ваш надежный партнер</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-2 offset-md-2">
            <div class="description-block__wrapper">
                <img class="description-block__image" src="/images/about/doc.jpg" alt="document">
                <div class="description-block__circle"></div>
                <div class="description-block__label-digit">9</div>
                <div class="description-block__label-txt">лет</div>

            </div>
        </div>
        <div class="col-12 col-md-6 h4 font-weight-light text-primary-light d-flex align-items-center ml-md-3 ml-lg-0 mt-sm-4 mt-md-0 text-center text-md-left">
            <!-- TODO Перевести в css -->
            Компания «ЛенТехСертификация» уже 9 лет выдает&nbsp;и&nbsp;помогает получить разрешительные документы для разных видов
            бизнеса.
        </div>
    </div>
</section>