@extends('components.review-block-template')
@section('differents')
    <div class="review-block__stars">
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
    </div>
    <div class="review-block__autor">
        <b>Дядька черномор,</b><br>
        директор ООО «Море волнуется раз»
    </div>
@endsection
