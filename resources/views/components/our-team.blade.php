<section class="company-team bg-gradient">
    <div class="container">
        <div class="row">
            <h2 class="col-12 text-center mb-md-7">Наша команда</h2>
            <div class="col-md-4 text-center mb-6 mb-md-0">
                <img class="company-team__img" src="/images/company-block/team-member.png" alt="photo">
                <h4 class="mb-3">Артём Робсон</h4>
                <span class="text-gray">Генеральный директор «ЛТС Групп»</span>
                <div class="row">
                    <div class="col-6 offset-3 company-team__social">
                        <a class="text-primary-light" href="#"><i class="fab fa-vk"></i></a>
                        <a class="text-primary-light" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="text-primary-light" href="#"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center mb-6 mb-md-0">
                <img class="company-team__img" src="/images/company-block/team-member.png" alt="photo">
                <h4 class="mb-3">Артём Робсон</h4>
                <span class="text-gray">Генеральный директор «ЛТС Групп»</span>
                <div class="row">
                    <div class="col-6 offset-3 company-team__social">
                        <a class="text-primary-light" href="#"><i class="fab fa-vk"></i></a>
                        <a class="text-primary-light" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="text-primary-light" href="#"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 text-center">
                <img class="company-team__img" src="/images/company-block/team-member.png" alt="photo">
                <h4 class="mb-3">Артём Робсон</h4>
                <span class="text-gray">Генеральный директор «ЛТС Групп»</span>
                <div class="row">
                    <div class="col-6 offset-3 company-team__social">
                        <a class="text-primary-light" href="#"><i class="fab fa-vk"></i></a>
                        <a class="text-primary-light" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="text-primary-light" href="#"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>