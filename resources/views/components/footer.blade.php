<footer class="footer">
    <div class="container page-block text-center text-md-left">
        <div class="row">
            <div class="col-md-3 mb-4 mb-md-0 d-flex flex-column align-items-center align-items-md-start">
                <div class="mb-3 mb-md-auto footer__brand">
                    <img src="/images/logo-white.svg" alt="logo">
                    <div class="footer__brand-label">Вопрос сертифицирования на нас</div>
                </div>
                <a href="#" class="small text-white">Политика конфиденциальности <br> и обработка данных</a>
            </div>
            <div class="col-md-3 subtitle-sm font-weight-bold mb-4 mb-md-0">
                <ul class="footer-list">
                    <li class="footer-list__item"><a class="footer-list__link" href="/services">Услуги</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="/projects">Проекты</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="/faq">Вопросы и ответы</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="/about">О Компании</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="/partners">Партнерам</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="/blog">Блог</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="/contacts">Контакты</a></li>
                </ul>
            </div>
            <div class="col-md-2 small mb-4 mb-md-0">
                <div>Бесплатно по России</div>
                <a href="tel:+78005120001" class="text-secondary font-weight-bold d-block mb-3">8 800 512 00 01</a>
                <div>Почта</div>
                <a href="mailto:admonistration@itsert.ru" class="font-weight-bold text-white d-block mb-6">admonistration@itsert.ru</a>
                <div class="footer-social">
                    <a class="footer-social__link" href="#"><i class="fab fa-vk"></i></a>
                    <a class="footer-social__link" href=""><i class="fab fa-facebook-f"></i></a>
                    <a class="footer-social__link" href=""><i class="fab fa-youtube"></i></a>
                </div>
            </div>
            <div class="col-md-2 offset-md-2 d-flex align-self-start">
                <button class="btn btn_sm btn_warning px-0 w-100"><i class="fas fa-phone-alt mr-2"></i> Заказать звонок
                </button>
            </div>
        </div>
    </div>
    <div class="footer__dev-link text-center small">
        Разработка и продвижение - <a class="text-secondary" href="#">Net-Simple.agency</a>
    </div>
</footer>
