<section class="container">
    <div class="promo-cert">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-1">
                <div class="promo-cert__incredible">НЕВЕРО<br>ЯТНЫЕ</div>
                <div class="promo-cert__x2">х2</div>
                <div class="h1 text-dark">При заказе любой услуги</div>
                <h4>
                    <span class="font-weight-normal">Добровольный пожарный сертификат</span><br>В ПОДАРОК
                </h4>
            </div>
            <form class="col-12 col-md-4">
                <div class="h4 text-center text-white mb-4">Круто, хочу подарок</div>
                <div class="input input_light">
                    <label for="promoPhone">Куда перезвонить?</label>
                    <input placeholder="Телефон" type="tel" id="promoPhone">
                </div>
                <div class="input input_light">
                    <label for="promoPhone">Что писать мы знаем, не знаем пока куда</label>
                    <input placeholder="Электронная почта" type="email" id="promoEmail">
                </div>
                <div class="input input_light mb-3">
                    <label for="promoMsg">Любые мысли по поводу и без. Вдруг станут классикой</label>
                    <textarea placeholder="Комментарий" id="promoMsg"></textarea>
                </div>
                <div class="input-cb input_light input-cb_sm">
                    <input type="checkbox" id="promoTerms" checked>
                    <label class="pr-5" for="promoTerms">
                        Я принимаю условия <a href="#">оферты</a> и <a href="#">пользовательского соглашения</a>
                    </label>
                </div>
                <button type="submit" class="btn btn_primary px-0 w-100">Получить&nbsp;подарок</button>
            </form>
        </div>
    </div>
</section>
