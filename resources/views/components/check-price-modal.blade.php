<div class="modal fade" id="checkPriceModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bg-secondary text-white">
            <div class="modal-body py-4 px-3">
                <div class="container-fluid">
                    <form class="row">
                        <div class="col">
                            <h4 class="text-center mb-3 font-weight-normal">Узнать стоимость</h4>
                            <div class="input input_light mb-3">
                                <label for="stayModalPhone">Куда перезвонить?</label>
                                <input placeholder="Телефон" id="stayModalPhone" data-maskphone>
                            </div>
                            <div class="input input_light">
                                <label for="promoPhone">Что писать мы знаем, не знаем пока куда</label>
                                <input placeholder="Электронная почта" type="email" id="promoEmail">
                            </div>
                            <div class="input input_light mb-3">
                                <label for="promoMsg">Любые мысли по поводу и без. Вдруг станут классикой</label>
                                <textarea placeholder="Комментарий" id="promoMsg"></textarea>
                            </div>
                            <div class="input-cb input_light input-cb_sm">
                                <input type="checkbox" id="promoTerms" checked>
                                <label class="pr-5" for="promoTerms">
                                    Я принимаю условия <a href="#">оферты</a> и <a href="#">пользовательского соглашения</a>
                                </label>
                            </div>
                            <button type="submit" class="btn btn_primary px-0 w-100">Узнать</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>