<section class="director-block">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5 d-flex justify-content-center align-items-lg-start justify-content-lg-end">
                <img class="director-block__photo h-100" src="/images/index/director.jpg">
            </div>
            <div class="col-12 col-lg-6 offset-lg-1">
                <blockquote class="page-block mb-0">
                    <p class="h3 mb-6 font-weight-light">Помогая другим — ты по&#8209;настоящему можешь быть
                        счастливым.
                    </p>
                    <footer class="d-flex flex-column flex-md-row align-items-start justify-content-between">
                        <div>
                            <div class="h3 text-primary-light">
                                Артём Робсон
                            </div>
                            <cite class="h5 text-gray">
                                Генеральный директор «ЛТС Групп»
                            </cite>
                        </div>
                        <img src="/images/index/director-sign.png"/>
                    </footer>
                </blockquote>
            </div>
        </div>
    </div>
</section>