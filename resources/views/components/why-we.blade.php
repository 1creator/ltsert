<section class="why-we-block page-block">
    <div class="container">
        <div class="row">
            <h2 class="col-12 text-center mb-md-7">
                Почему нас выбирают?
            </h2>
        </div>
        <div class="row">
            <div class="col-md-4 mb-6">
                <div class="why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-shield-alt"></i>
                    </div>
                    <h3 class="text-primary">С нами надежно</h3>
                    <div>В случае форс-мажора мы готовы вернуть деньги в полном объеме! Закреплено в договоре.</div>
                </div>
            </div>
            <div class="col-md-4 px-md-3 mb-6">
                <div class="why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-check"></i>
                    </div>
                    <h3 class="text-primary">Все официально</h3>
                    <div>Никаких подделок! Ваш сертификат заносится в реестр государственной аккредитации.</div>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="why-we-block__card px-md-3">
                    <div class="why-we-block__img">
                        <i class="fas fa-tachometer-alt"></i>
                    </div>
                    <h3 class="text-primary">Оперативно</h3>
                    <div>Мы подготовим макет за несколько часов при условии предоставления всех требуемых документов и
                        оперативном согласовании.</div>
                </div>
            </div>
            <div class="col-md-4 mb-6 mb-md-0">
                <div class="why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-shipping-fast"></i>
                    </div>
                    <h3 class="text-primary">Бесплатная доставка</h3>
                    <div>Курьер доставит оригинал документа прямо в офис совершенно бесплатно!</div>
                </div>
            </div>
            <div class="col-md-4 mb-6 mb-md-0">
                <div class="why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-clock"></i>
                    </div>
                    <h3 class="text-primary">Экономим ваше время</h3>
                    <div>Мы сами заполним заявку на получение сертификата. Вам нужно будет просто предоставить нам
                        информацию
                        и документы.</div>
                </div>
            </div>
            <div class="col-md-4 mb-0">
                <div class="why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-comments"></i>
                    </div>
                    <h3 class="text-primary">Поможем разобраться</h3>
                    <div>У нас вы бесплатно получите подробную консультацию по оформлению и получению документации.</div>
                </div>
            </div>
        </div>
    </div>
</section>