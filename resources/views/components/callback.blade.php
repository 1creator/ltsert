<section class="callback-block container">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2">
            <div class="text-center text-gray">
                @isset($h4)
                    <div class="h4 font-weight-light mb-2">{{$h4}}</div>
                @endisset
                @isset($h5)
                    <div class="h5 font-weight-light mb-3">{{$h5}}</div>
                @endisset
            </div>
            <div class="row align-items-end no-gutters">
                <div class="col-12 col-md-6 mb-4 mb-md-0 pr-md-2">
                    <div class="input mb-0">
                        @php($id = Str::random(2))
                        <label for="callback_{{$id}}">Куда перезвонить?</label>
                        <input placeholder="Телефон" id="callback_{{$id}}" data-maskphone>
                    </div>
                </div>
                <div class="col-12 col-md-6 pl-md-1">
                    <button class="btn btn_secondary w-100">Жду звонка</button>
                </div>
            </div>
        </div>
    </div>
</section>