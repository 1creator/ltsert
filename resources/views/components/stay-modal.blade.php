<div class="modal stay-modal fade" id="stayModal">
    <div class="modal-dialog modal-dialog-centered stay-modal__dialog" role="document">
        <div class="modal-content stay-modal__content">
            <div class="modal-body p-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col stay-modal__left">
                        </div>
                        <div class="col stay-modal__right">
                            <div class="h2 mb-3 text-center">Пожалуйста, не уходите!</div>
                            <div class="h4 text-center mb-4 font-weight-normal">Мы вам перезвоним</div>
                            <div class="input input_light mb-3">
                                <label for="stayModalPhone">Куда перезвонить?</label>
                                <input placeholder="Телефон" id="stayModalPhone" data-maskphone>
                            </div>
                            <div class="input-cb input_light input-cb_sm ml-3 mr-6 mb-3">
                                <input type="checkbox" id="stayModalTerms" checked>
                                <label for="stayModalTerms">
                                    Я принимаю условия <a href="#" class="text-primary">оферты</a> и
                                    <a href="#" class="text-primary">пользовательского
                                        соглашения</a>
                                </label>
                            </div>
                            <button class="btn btn_primary w-100">Перезвоните мне</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>