<section class="cases-block page-block bg-gray-light-ultra">
    <div class="container">
        <h2 class="text-center mb-6">Релевантные кейсы</h2>
        <div class="row mb-5">
            <div class="col-12 col-lg-4 mb-4 mb-lg-0">
                <article class="article-card">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-1.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date">08.11.2019</div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Обучение по охране труда: от безопасности сотрудников к соблюдению закона</a>
                    </h3>
                    <p class="article-card__text">
                        Обучение по охране труда призвано уберечь персонал от несчастных случаев на рабочем месте,
                        благодаря подробному инструктированию с последующей проверкой полученных знаний. ...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4 mb-4 mb-lg-0">
                <article class="article-card">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-2.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date">08.11.2019</div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация трансформаторов: нормативные акты, необходимые документы...</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты действуют в отношении различных видов трансформаторов? Как получить
                        сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты компании...
                    </p>
                </article>
            </div>
            <div class="col-12 col-lg-4 mb-4 mb-lg-0">
                <article class="article-card">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-3.jpg')"
                    >
                    </div>
                    <div class="article-card__top-wrap">
                        <div class="article-card__date">08.11.2019</div>
                        <div class="article-card__company">
                            <i class="fas fa-users mr-2"></i>ЛенТехСертификация
                        </div>
                        <div class="article-card__read-time">
                            <i class="fas fa-eye mr-2"></i>Время чтения: 7 минут
                        </div>
                    </div>
                    <h3 class="article-card__title">
                        <a href="#">Сертификация кондиционеров: как подтверждается соответствие сплит систем?</a>
                    </h3>
                    <p class="article-card__text">
                        Какие нормативные акты в области сертификации кондиционеров актуальны? Как получить сертификат
                        соответствия на кондиционер? Какие документы необходимо собрать для подачи ...
                    </p>
                </article>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 offset-md-4">
                <a href="#" class="btn btn_primary btn_outline px-0 w-100">Смотреть все кейсы</a>
            </div>
        </div>
    </div>
</section>