<section class="callback-primary-block bg-gradient-primary">
    <div class="container">
        <form class="row">
            <div class="col-12 col-lg-4 align-self-center">
                <h2 class="text-center">
                    Запросить <span class="text-secondary">бесплатную</span> консультацию
                </h2>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="input">
                    <label for="callbackPhone">Куда перезвонить?</label>
                    <input placeholder="Телефон" type="tel" id="callbackPhone" data-maskphone>
                </div>
                <div class="input">
                    <label for="callbackEmail">Что писать мы знаем, не знаем пока куда</label>
                    <input placeholder="Электронная почта" type="email" id="callbackEmail">
                </div>
                <div class="input-cb input-cb_sm ml-lg-5 mb-md-0">
                    <input type="checkbox" id="callbackTerms" checked>
                    <label for="callbackTerms">
                        Я принимаю условия <a href="#">оферты</a> и
                        <a href="#" >пользовательского
                            соглашения</a>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="input">
                    <label for="callbackMsg">Любые мысли по поводу и без. Вдруг станут классикой</label>
                    <textarea placeholder="Комментарий" id="callbackMsg"
                              rows="3"></textarea>
                </div>
                <button class="btn btn_secondary px-0 w-100">Запросить бесплатно</button>
            </div>
        </form>
    </div>
</section>