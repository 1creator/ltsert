<section class="page-block container">
    <h2 class="text-center">Отзывы</h2>
    <div class="row mb-5">
        <div class="col-12 col-lg-4 mb-3 mb-md-0">
            <a class="review-card review-card_image" data-fancybox href="/images/service/review-image.png">
                <div class="review-card__image"
                     style="background-image: url('/images/service/review-image.png')">
                </div>
                <div class="review-card__gradient"></div>
                <div class="review-card__date">08.11.2019</div>
            </a>
        </div>
        <div class="col-12 col-lg-4 mb-3 mb-md-0">
            <div class="review-card review-card_video"
                 style="background-image: url('/images/service/review-video-preview.jpg')">
                <div class="review-card__date">08.11.2019</div>
                <a data-fancybox href="https://www.youtube.com/watch?v=uJo6GnDYi7Y">
                    <button class="fas fa-play-circle review-card__play-btn"></button>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <blockquote class="review-card">
                <p class="mb-6">
                    ООО "ЛенТехСертификация" зарекомендовало себя надежным деловым партнером, обладающим большим
                    потенциалом
                    для решения соответствующих задач. Данная компания является редчайшим примером идеального партнера.
                    За
                    время сотрудничества не было никаких задержек и ошибок, все всегда вовремя и четко.
                </p>
                <footer class="subtitle text-primary">Благодарственное письмо ООО «Тюменьсвязьфлот»
                </footer>
            </blockquote>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4 offset-md-4">
            <a class="btn btn_primary btn_outline w-100" href="#">Все отзывы</a>
        </div>
    </div>
</section>