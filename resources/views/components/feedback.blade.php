<section class="feedback-block bg-gray-light-ultra">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 mb-4 text-center">
                <p class="h4 mb-4 font-weight-normal mx-md-n2">
                    Сообщите нам своё впечатление о работе компании или отдельного сотрудника:
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-2 offset-md-3 mb-3 mb-md-0">
                <button class="btn btn_secondary btn_sm px-0 w-100">
                    <i class="fas fa-heart mr-2"></i> Сказать спасибо
                </button>
            </div>
            <div class="col-12 col-md-2 mb-3 mb-md-0">
                <button class="btn btn_danger btn_sm px-0 w-100">
                    <i class="fas fa-exclamation-circle mr-2"></i>Пожаловаться
                </button>
            </div>
            <div class="col-12 col-md-2 mb-3 mb-md-0">
                <button class="btn btn_warning btn_sm px-0 w-100">
                    <i class="fas fa-question-circle mr-2"></i>Задать вопрос
                </button>
            </div>
        </div>
    </div>
</section>