@extends('layout')
@push('head')
    <title>Партнерам</title>
@endpush
@section('content')
    <section class="entry-block partners__entry-block bg-gradient-light">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-7">
                    <h1 class="h2">
                        Станьте партнером «Лен<span class="d-lg-none">&shy;</span>ТехСертификация» — экономьте или
                        зарабатывайте!
                    </h1>
                </div>
                <div class="col-12 col-md-4 my-md-n6">
                    <div class="input input_dark">
                        <label for="promoPhone">Куда перезвонить?</label>
                        <input placeholder="Телефон" type="tel" id="promoPhone" data-maskphone>
                    </div>
                    <div class="input input_dark">
                        <label for="promoEmail">Что писать мы знаем, не знаем пока куда</label>
                        <input placeholder="Электронная почта" type="email" id="promoEmail">
                    </div>
                    <div class="input input_dark mb-3">
                        <label for="promoMsg">Любые мысли по поводу и без. Вдруг станут классикой</label>
                        <textarea placeholder="Комментарий" id="promoMsg"></textarea>
                    </div>
                    <div class="input-cb input-cb_sm input-cb_dark">
                        <input type="checkbox" id="promoTerms" checked>
                        <label class="pr-5" for="promoTerms" c>
                            Я принимаю условия <a href="#">оферты</a> и <a href="#">пользовательского соглашения</a>
                        </label>
                    </div>
                    <button type="submit" class="btn btn_secondary px-0 w-100">ЗАПРОСИТЬ БЕСПЛАТНО</button>
                </div>
            </div>
        </div>
    </section>
    @include('components.breadcrumbs',['items'=>[
        'Главная'=>'/',
        'Партнёрам'=>'/partners',
    ]])
    <section class="container page-block">
        <h2 class="text-center mb-md-7">
            Приглашаем к сотрудничеству
        </h2>
        <div class="row">
            <div class="col-md-4 mb-6">
                <div class=" why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-check"></i>
                    </div>
                    <h3 class="text-primary">Таможенных брокеров</h3>
                    <p>В случае форс-мажора мы готовы вернуть деньги в полном объеме! Закреплено в договоре.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6 px-md-3">
                <div class=" why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-check"></i>
                    </div>
                    <h3 class="text-primary">Заказчиков</h3>
                    <p>Никаких подделок! Ваш сертификат заносится в реестр государственной аккредитации.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class=" why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-check"></i>
                    </div>
                    <h3 class="text-primary">Сотрудников компаний</h3>
                    <p>Мы подготовим макет за несколько часов при условии предоставления всех требуемых документов и
                        оперативном согласовании.</p>
                </div>
            </div>
            <div class="col-md-4 offset-md-2 mb-6 mb-md-0">
                <div class=" why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-check"></i>
                    </div>
                    <h3 class="text-primary">Бизнес-партнеров</h3>
                    <p>Курьер доставит оригинал документа прямо в офис совершенно бесплатно!</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class=" why-we-block__card">
                    <div class="why-we-block__img">
                        <i class="fas fa-check"></i>
                    </div>
                    <h3 class="text-primary">Агентов</h3>
                    <p>Мы сами заполним заявку на получение сертификата. Вам нужно будет просто предоставить нам
                        информацию<br class="d-none d-md-block"/>
                        и документы.</p>
                </div>
            </div>
        </div>
    </section>
    <div class="bg-gradient">
        @include('components.your-safe-partner')
        @include('components.our-directions')
    </div>
    @include('components.company-principles', ['class'=>'principles-block_bg-gradient'])
    @include('components.why-we')
    @include('components.our-team')
    @include('components.certificates')
    @include('components.callback-primary')
    @include('components.feedback')
@endsection
